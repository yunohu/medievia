/variable {counter} {1} {TVVariables}
/gag {We will pay you %3 gold for each %4} {TVgag}
/alias {valall} {/enablegroup {TVtrigs};buy pack horse} {TValias}

/action {You pay 20000 gold and take control of your pack horse.} {/disablegroup {TVtrigs};/enablegroup {TVgag};/enablegroup {TVPosts};/enablegroup {TVtriggers};look;/loop {1,88} {value @GetArray(itstf,$LoopCount,1)};sell pack horse} {TVtrigs}
/action {You already have freight, you must abandon or hitch it first} {/showme {@AnsiBold()@ForeGreen()You already have a current freight, you must hitch or abandon it before valuing.@AnsiReset()@Chr(10)};/disablegroup {TVtrigs}} {TVtrigs}

/alias {tvhelp} {/showme {@Chr(10)@AnsiBold()@ForeYellow()Help File for @AnsiBold()@ForeGreen()Trade Value@AnsiReset()};/showme {@AnsiBold()@ForeYellow()--------------------------------------------------------------------------------@AnsiReset()};/showme {@AnsiReset()@AnsiBold()@ForeGreen()valall                       @ForeBlue()- @ForeCyan()Type This First! This alias will get the values@AnsiReset()};/showme {                               @AnsiBold()@ForeCyan()of each item from the trade post you are in.@AnsiReset()};/showme {@Chr(10)@AnsiBold()@ForeGreen()showprices @ForeRed()<@ForeYellow()#@ForeRed()>               @ForeBlue()- @ForeCyan()This alias will print out the current values@AnsiReset()};/showme {                               @AnsiBold()@ForeCyan()for each item that is sold at @ForeMagenta()other @ForeCyan()trade@AnsiReset()};/showme { @ForeCyan()showprices 1000000            @AnsiBold()@ForeCyan()posts.  Optionally, you can specify a minimum@AnsiReset()};/showme {                               @AnsiBold()@ForeCyan()value to be displayed.  PPM is Profit Per Move.@AnsiReset()};/showme {@Chr(10)};/showme {@AnsiBold()@ForeGreen()showppm @ForeRed()<@ForeYellow()#@ForeRed()>                  @ForeBlue()- @ForeCyan()This alias will print out the current values@AnsiReset()};/showme {                               @AnsiBold()@ForeCyan()for each item that is sold at @ForeMagenta()other @ForeCyan()trade@AnsiReset()};/showme { @ForeCyan() showppm 450                  @AnsiBold()@ForeCyan()posts where PPM is higher than the specified@AnsiReset()};/showme {                               @AnsiBold()@ForeCyan()value.  Optionally, you can specify a minimum@AnsiReset()};/showme {                               @AnsiBold()@ForeCyan()PPM to sort on.  PPM is profit per move.@AnsiReset()};/showme {@Chr(10)};/showme {@AnsiBold()@ForeGreen()showpp @ForeRed()<@ForeYellow()# #@ForeRed()>                 @ForeBlue()- @ForeCyan()This alias will print out the current values@Chr(10)                               for each item that is sold at @ForeMagenta()other@ForeCyan() trade@Chr(10)@AnsiReset()@ForeCyan() showpp 1000000 450            @AnsiBold()@ForeCyan()posts where Profit and PPM are higher than the@Chr(10)                               specified values.  Optionally, you can specify@Chr(10)                               minimum values to sort on.@AnsiReset()};/showme {@Chr(10)};/showme {@AnsiBold()@ForeGreen()values @ForeRed()<@ForeYellow()string@ForeRed()> <@ForeYellow()channel@ForeRed()>    @ForeBlue()- @ForeCyan()This alias takes the first 3 letters of the @ForeMagenta()zone@AnsiReset()};/showme {                               @AnsiBold()@ForeCyan()the trade post is at, and then will chat the@AnsiReset()};/showme { @ForeCyan()values riv clan               @AnsiBold()@ForeCyan()values to the specified channel.  With no@AnsiReset()};/showme { @ForeCyan()values list                   @AnsiBold()@ForeCyan()arguments, it will give you the syntax.  Type@AnsiReset()};/showme {                               @AnsiBold()@ForeMagenta()values list@ForeCyan() for a list of trade posts.};/showme {@Chr(10)@AnsiBold()@ForeGreen()vchat @ForeRed()<@ForeYellow()string@ForeRed()> <@ForeYellow()chatname@ForeRed()>  @ForeBlue()  - @ForeCyan()This alias sends values for @ForeMagenta()zone@ForeCyan() to @ForeMagenta()chatname@ForeCyan().@AnsiReset()};/showme {@Chr(10)@AnsiBold()@ForeGreen()vchatall @ForeRed()<@ForeYellow()string@ForeRed()>            @ForeBlue()- @ForeCyan()This alias sends values for @ForeMagenta()zone @ForeCyan()to all of your@AnsiReset()};/showme {                               @AnsiBold()@ForeMagenta()chat connections@ForeCyan().@AnsiReset()};/showme {@Chr(10)@AnsiBold()@ForeRed()Trade Values Version @ForeCyan()1.95@ForeRed() By@ForeYellow() Kernighan.@Chr(10)@ForeYellow()http://www.geocities.com/mishikal/@Chr(10)@ForeGreen()Idea from a similar script for ZMud by Carabas.@AnsiReset()}} {TValias}

/alias {values %1} {/if {@IsEmpty($1)==1} {/showme {@Chr(10)@AnsiBold()@ForeYellow()Syntax: @ForeRed()values @ForeGreen()tradepost command @ForeYellow()- @ForeGreen()tradepost @ForeYellow()must be the first three or more letters and @ForeGreen()command @ForeYellow()can be anything.@AnsiReset()}} {/if {$1 == "list"} {/showme {@Chr(10)@AnsiBold()@ForeYellow()Medievia Trade Posts@AnsiReset()};/showme {@AnsiBold()@ForeYellow()--------------------};/loop {1,18} {/showme {@AnsiBold()@ForeGreen()@GetArray(shopnames,$LoopCount,0)@AnsiReset()}}} {/var {Shop} {@Word($1,1)};/var {NumWordLength} {@Len($Shop)};/var {NumStringLength} {@Len($1)};/math {NumChars} {$NumStringLength - $NumWordLength};/var {Command} {@Mid($1,$NumWordLength,$NumChars)};/if {$NumWordLength < 3} {/showme {@AnsiBold()@ForeCyan()Shop argument must be 3 letters or more long.@AnsiReset()}} {/if {@StrStr(@Lower(@GetArray(shopnames,1,0)),@Lower(@Word($1,1))) != -1} {$Command @GetArray(shopnames, 1,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,1,1) yards] @GetArray(itstf,1,1): @GetArray(itstf,1,7). @GetArray(itstf,2,1): @GetArray(itstf,2,7). @GetArray(itstf,3,1): @GetArray(itstf,3,7). @GetArray(itstf,4,1): @GetArray(itstf,4,7). @GetArray(itstf,5,1): @GetArray(itstf,5,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,2,0)),@Lower(@Word($1,1))) != -1} {$Command @GetArray(shopnames,2,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,2,1) yards] @GetArray(itstf,6,1): @GetArray(itstf,6,7). @GetArray(itstf,7,1): @GetArray(itstf,7,7). @GetArray(itstf,8,1): @GetArray(itstf,8,7)} {/if {@StrStr(@Lower(@GetArray(shopnames,3,0)),@Lower(@Word($1,1))) != -1} {$Command @GetArray(shopnames,3,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,3,1) yards] @GetArray(itstf,9,1): @GetArray(itstf,9,7). @GetArray(itstf,10,1): @GetArray(itstf,10,7). @GetArray(itstf,11,1): @GetArray(itstf,11,7). @GetArray(itstf,12,1): @GetArray(itstf,12,7). @GetArray(itstf,13,1): @GetArray(itstf,13,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,4,0)),@Lower(@Word($1,1))) != -1} {$Command @GetArray(shopnames,4,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,4,1) yards] @GetArray(itstf,14,1): @GetArray(itstf,14,7). @GetArray(itstf,15,1): @GetArray(itstf,15,7). @GetArray(itstf,16,1): @GetArray(itstf,16,7). @GetArray(itstf,17,1): @GetArray(itstf,17,7). @GetArray(itstf,18,1): @GetArray(itstf,18,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,5,0)),@Lower(@Word($1,1))) != -1} {$Command @GetArray(shopnames,5,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,5,1) yards] @GetArray(itstf,19,1): @GetArray(itstf,19,7). @GetArray(itstf,20,1): @GetArray(itstf,20,7). @GetArray(itstf,21,1): @GetArray(itstf,21,7). @GetArray(itstf,22,1): @GetArray(itstf,22,7). @GetArray(itstf,23,1): @GetArray(itstf,23,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,6,0)),@Lower(@Word($1,1))) != -1} {$Command @GetArray(shopnames,6,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,6,1) yards] @GetArray(itstf,24,1): @GetArray(itstf,24,7). @GetArray(itstf,25,1): @GetArray(itstf,25,7). @GetArray(itstf,26,1): @GetArray(itstf,26,7). @GetArray(itstf,27,1): @GetArray(itstf,27,7). @GetArray(itstf,28,1): @GetArray(itstf,28,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,7,0)),@Lower(@Word($1,1))) != -1} {$Command @GetArray(shopnames,7,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,7,1) yards] @GetArray(itstf,29,1): @GetArray(itstf,29,7). @GetArray(itstf,30,1): @GetArray(itstf,30,7). @GetArray(itstf,31,1): @GetArray(itstf,31,7). @GetArray(itstf,32,1): @GetArray(itstf,32,7). @GetArray(itstf,33,1): @GetArray(itstf,33,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,8,0)),@Lower(@Word($1,1))) != -1} {$Command @GetArray(shopnames,8,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,8,1) yards] @GetArray(itstf,34,1): @GetArray(itstf,34,7). @GetArray(itstf,35,1): @GetArray(itstf,35,7). @GetArray(itstf,36,1): @GetArray(itstf,36,7). @GetArray(itstf,37,1): @GetArray(itstf,37,7). @GetArray(itstf,38,1): @GetArray(itstf,38,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,9,0)),@Lower(@Word($1,1))) != -1} {$Command @GetArray(shopnames,9,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,9,1) yards] @GetArray(itstf,39,1): @GetArray(itstf,39,7). @GetArray(itstf,40,1): @GetArray(itstf,40,7). @GetArray(itstf,41,1): @GetArray(itstf,41,7). @GetArray(itstf,42,1): @GetArray(itstf,42,7). @GetArray(itstf,43,1): @GetArray(itstf,43,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,10,0)),@Lower(@Word($1,1))) != -1} {$Command @GetArray(shopnames,10,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,10,1) yards] @GetArray(itstf,44,1): @GetArray(itstf,44,7). @GetArray(itstf,45,1): @GetArray(itstf,45,7). @GetArray(itstf,46,1): @GetArray(itstf,46,7). @GetArray(itstf,47,1): @GetArray(itstf,47,7). @GetArray(itstf,48,1): @GetArray(itstf,48,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,11,0)),@Lower(@Word($1,1))) != -1} {$Command @GetArray(shopnames,11,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,11,1) yards] @GetArray(itstf,49,1): @GetArray(itstf,49,7). @GetArray(itstf,50,1): @GetArray(itstf,50,7). @GetArray(itstf,51,1): @GetArray(itstf,51,7). @GetArray(itstf,52,1): @GetArray(itstf,52,7). @GetArray(itstf,53,1): @GetArray(itstf,53,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,12,0)),@Lower(@Word($1,1))) != -1} {$Command @GetArray(shopnames,12,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,12,1) yards] @GetArray(itstf,54,1): @GetArray(itstf,54,7). @GetArray(itstf,55,1): @GetArray(itstf,55,7). @GetArray(itstf,56,1): @GetArray(itstf,56,7). @GetArray(itstf,57,1): @GetArray(itstf,57,7). @GetArray(itstf,58,1): @GetArray(itstf,58,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,13,0)),@Lower(@Word($1,1))) != -1} {$Command @GetArray(shopnames,13,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,13,1) yards] @GetArray(itstf,59,1): @GetArray(itstf,59,7). @GetArray(itstf,60,1): @GetArray(itstf,60,7). @GetArray(itstf,61,1): @GetArray(itstf,61,7). @GetArray(itstf,62,1): @GetArray(itstf,62,7). @GetArray(itstf,63,1): @GetArray(itstf,63,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,14,0)),@Lower(@Word($1,1))) != -1} {$Command @GetArray(shopnames,14,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,14,1) yards] @GetArray(itstf,64,1): @GetArray(itstf,64,7). @GetArray(itstf,65,1): @GetArray(itstf,65,7). @GetArray(itstf,66,1): @GetArray(itstf,66,7). @GetArray(itstf,67,1): @GetArray(itstf,67,7). @GetArray(itstf,68,1): @GetArray(itstf,68,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,15,0)),@Lower(@Word($1,1))) != -1} {$Command @GetArray(shopnames,15,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,15,1) yards] @GetArray(itstf,69,1): @GetArray(itstf,69,7). @GetArray(itstf,70,1): @GetArray(itstf,70,7). @GetArray(itstf,71,1): @GetArray(itstf,71,7). @GetArray(itstf,72,1): @GetArray(itstf,72,7). @GetArray(itstf,73,1): @GetArray(itstf,73,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,16,0)),@Lower(@Word($1,1))) != -1} {$Command @GetArray(shopnames,16,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,16,1) yards] @GetArray(itstf,74,1): @GetArray(itstf,74,7). @GetArray(itstf,75,1): @GetArray(itstf,75,7). @GetArray(itstf,76,1): @GetArray(itstf,76,7). @GetArray(itstf,77,1): @GetArray(itstf,77,7). @GetArray(itstf,78,1): @GetArray(itstf,78,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,17,0)),@Lower(@Word($1,1))) != -1} {$Command @GetArray(shopnames,17,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,17,1) yards] @GetArray(itstf,79,1): @GetArray(itstf,79,7). @GetArray(itstf,80,1): @GetArray(itstf,80,7). @GetArray(itstf,81,1): @GetArray(itstf,81,7). @GetArray(itstf,82,1): @GetArray(itstf,82,7). @GetArray(itstf,83,1): @GetArray(itstf,83,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,18,0)),@Lower(@Word($1,1))) != -1} {$Command @GetArray(shopnames,18,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,18,1) yards] @GetArray(itstf,84,1): @GetArray(itstf,84,7). @GetArray(itstf,85,1): @GetArray(itstf,85,7). @GetArray(itstf,86,1): @GetArray(itstf,86,7). @GetArray(itstf,87,1): @GetArray(itstf,87,7). @GetArray(itstf,88,1): @GetArray(itstf,88,7).}}}}}}}}}}}}}}}}}}}}}} {TValias}

/alias {vchat %1} {/if {@IsEmpty($1)==1} {/showme {@Chr(10)@AnsiBold()@ForeYellow()Syntax: @ForeRed()values @ForeGreen()tradepost command @ForeYellow()- @ForeGreen()tradepost @ForeYellow()must be the first three or more letters and @ForeGreen()command @ForeYellow()can be anything.@AnsiReset()}} {/if {$1 == "list"} {/showme {@Chr(10)@AnsiBold()@ForeYellow()Medievia Trade Posts@AnsiReset()};/showme {@AnsiBold()@ForeYellow()--------------------};/loop {1,18} {/showme {@AnsiBold()@ForeGreen()@GetArray(shopnames,$LoopCount,0)@AnsiReset()}}} {/var {Shop} {@Word($1,1)};/var {NumWordLength} {@Len($Shop)};/var {NumStringLength} {@Len($1)};/math {NumChars} {$NumStringLength - $NumWordLength};/var {Command} {@Mid($1,$NumWordLength,$NumChars)};/if {$NumWordLength < 3} {/showme {@AnsiBold()@ForeCyan()Shop argument must be 3 letters or more long.@AnsiReset()}} {/if {@StrStr(@Lower(@GetArray(shopnames,1,0)),@Lower(@Word($1,1))) != -1} {/chat $Command @GetArray(shopnames, 1,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,1,1) yards] @GetArray(itstf,1,1): @GetArray(itstf,1,7). @GetArray(itstf,2,1): @GetArray(itstf,2,7). @GetArray(itstf,3,1): @GetArray(itstf,3,7). @GetArray(itstf,4,1): @GetArray(itstf,4,7). @GetArray(itstf,5,1): @GetArray(itstf,5,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,2,0)),@Lower(@Word($1,1))) != -1} {/chat $Command @GetArray(shopnames,2,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,2,1) yards] @GetArray(itstf,6,1): @GetArray(itstf,6,7). @GetArray(itstf,7,1): @GetArray(itstf,7,7). @GetArray(itstf,8,1): @GetArray(itstf,8,7)} {/if {@StrStr(@Lower(@GetArray(shopnames,3,0)),@Lower(@Word($1,1))) != -1} {/chat $Command @GetArray(shopnames,3,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,3,1) yards] @GetArray(itstf,9,1): @GetArray(itstf,9,7). @GetArray(itstf,10,1): @GetArray(itstf,10,7). @GetArray(itstf,11,1): @GetArray(itstf,11,7). @GetArray(itstf,12,1): @GetArray(itstf,12,7). @GetArray(itstf,13,1): @GetArray(itstf,13,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,4,0)),@Lower(@Word($1,1))) != -1} {/chat $Command @GetArray(shopnames,4,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,4,1) yards] @GetArray(itstf,14,1): @GetArray(itstf,14,7). @GetArray(itstf,15,1): @GetArray(itstf,15,7). @GetArray(itstf,16,1): @GetArray(itstf,16,7). @GetArray(itstf,17,1): @GetArray(itstf,17,7). @GetArray(itstf,18,1): @GetArray(itstf,18,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,5,0)),@Lower(@Word($1,1))) != -1} {/chat $Command @GetArray(shopnames,5,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,5,1) yards] @GetArray(itstf,19,1): @GetArray(itstf,19,7). @GetArray(itstf,20,1): @GetArray(itstf,20,7). @GetArray(itstf,21,1): @GetArray(itstf,21,7). @GetArray(itstf,22,1): @GetArray(itstf,22,7). @GetArray(itstf,23,1): @GetArray(itstf,23,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,6,0)),@Lower(@Word($1,1))) != -1} {/chat $Command @GetArray(shopnames,6,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,6,1) yards] @GetArray(itstf,24,1): @GetArray(itstf,24,7). @GetArray(itstf,25,1): @GetArray(itstf,25,7). @GetArray(itstf,26,1): @GetArray(itstf,26,7). @GetArray(itstf,27,1): @GetArray(itstf,27,7). @GetArray(itstf,28,1): @GetArray(itstf,28,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,7,0)),@Lower(@Word($1,1))) != -1} {/chat $Command @GetArray(shopnames,7,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,7,1) yards] @GetArray(itstf,29,1): @GetArray(itstf,29,7). @GetArray(itstf,30,1): @GetArray(itstf,30,7). @GetArray(itstf,31,1): @GetArray(itstf,31,7). @GetArray(itstf,32,1): @GetArray(itstf,32,7). @GetArray(itstf,33,1): @GetArray(itstf,33,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,8,0)),@Lower(@Word($1,1))) != -1} {/chat $Command @GetArray(shopnames,8,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,8,1) yards] @GetArray(itstf,34,1): @GetArray(itstf,34,7). @GetArray(itstf,35,1): @GetArray(itstf,35,7). @GetArray(itstf,36,1): @GetArray(itstf,36,7). @GetArray(itstf,37,1): @GetArray(itstf,37,7). @GetArray(itstf,38,1): @GetArray(itstf,38,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,9,0)),@Lower(@Word($1,1))) != -1} {/chat $Command @GetArray(shopnames,9,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,9,1) yards] @GetArray(itstf,39,1): @GetArray(itstf,39,7). @GetArray(itstf,40,1): @GetArray(itstf,40,7). @GetArray(itstf,41,1): @GetArray(itstf,41,7). @GetArray(itstf,42,1): @GetArray(itstf,42,7). @GetArray(itstf,43,1): @GetArray(itstf,43,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,10,0)),@Lower(@Word($1,1))) != -1} {/chat $Command @GetArray(shopname,10,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,10,1) yards] @GetArray(itstf,44,1): @GetArray(itstf,44,7). @GetArray(itstf,45,1): @GetArray(itstf,45,7). @GetArray(itstf,46,1): @GetArray(itstf,46,7). @GetArray(itstf,47,1): @GetArray(itstf,47,7). @GetArray(itstf,48,1): @GetArray(itstf,48,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,11,0)),@Lower(@Word($1,1))) != -1} {/chat $Command @GetArray(shopnames,11,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,11,1) yards] @GetArray(itstf,49,1): @GetArray(itstf,49,7). @GetArray(itstf,50,1): @GetArray(itstf,50,7). @GetArray(itstf,51,1): @GetArray(itstf,51,7). @GetArray(itstf,52,1): @GetArray(itstf,52,7). @GetArray(itstf,53,1): @GetArray(itstf,53,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,12,0)),@Lower(@Word($1,1))) != -1} {/chat $Command @GetArray(shopnames,12,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,12,1) yards] @GetArray(itstf,54,1): @GetArray(itstf,54,7). @GetArray(itstf,55,1): @GetArray(itstf,55,7). @GetArray(itstf,56,1): @GetArray(itstf,56,7). @GetArray(itstf,57,1): @GetArray(itstf,57,7). @GetArray(itstf,58,1): @GetArray(itstf,58,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,13,0)),@Lower(@Word($1,1))) != -1} {/chat $Command @GetArray(shopnames,13,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,13,1) yards] @GetArray(itstf,59,1): @GetArray(itstf,59,7). @GetArray(itstf,60,1): @GetArray(itstf,60,7). @GetArray(itstf,61,1): @GetArray(itstf,61,7). @GetArray(itstf,62,1): @GetArray(itstf,62,7). @GetArray(itstf,63,1): @GetArray(itstf,63,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,14,0)),@Lower(@Word($1,1))) != -1} {/chat $Command @GetArray(shopnames,14,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,14,1) yards] @GetArray(itstf,64,1): @GetArray(itstf,64,7). @GetArray(itstf,65,1): @GetArray(itstf,65,7). @GetArray(itstf,66,1): @GetArray(itstf,66,7). @GetArray(itstf,67,1): @GetArray(itstf,67,7). @GetArray(itstf,68,1): @GetArray(itstf,68,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,15,0)),@Lower(@Word($1,1))) != -1} {/chat $Command @GetArray(shopnames,15,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,15,1) yards] @GetArray(itstf,69,1): @GetArray(itstf,69,7). @GetArray(itstf,70,1): @GetArray(itstf,70,7). @GetArray(itstf,71,1): @GetArray(itstf,71,7). @GetArray(itstf,72,1): @GetArray(itstf,72,7). @GetArray(itstf,73,1): @GetArray(itstf,73,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,16,0)),@Lower(@Word($1,1))) != -1} {/chat $Command @GetArray(shopnames,16,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,16,1) yards] @GetArray(itstf,74,1): @GetArray(itstf,74,7). @GetArray(itstf,75,1): @GetArray(itstf,75,7). @GetArray(itstf,76,1): @GetArray(itstf,76,7). @GetArray(itstf,77,1): @GetArray(itstf,77,7). @GetArray(itstf,78,1): @GetArray(itstf,78,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,17,0)),@Lower(@Word($1,1))) != -1} {/chat $Command @GetArray(shopname,17,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,17,1) yards] @GetArray(itstf,79,1): @GetArray(itstf,79,7). @GetArray(itstf,80,1): @GetArray(itstf,80,7). @GetArray(itstf,81,1): @GetArray(itstf,81,7). @GetArray(itstf,82,1): @GetArray(itstf,82,7). @GetArray(itstf,83,1): @GetArray(itstf,83,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,18,0)),@Lower(@Word($1,1))) != -1} {/chat $Command @GetArray(shopnames,18,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,18,1) yards] @GetArray(itstf,84,1): @GetArray(itstf,84,7). @GetArray(itstf,85,1): @GetArray(itstf,85,7). @GetArray(itstf,86,1): @GetArray(itstf,86,7). @GetArray(itstf,87,1): @GetArray(itstf,87,7). @GetArray(itstf,88,1): @GetArray(itstf,88,7).}}}}}}}}}}}}}}}}}}}}}} {TValias}

/alias {vchatall %1} {/if {@IsEmpty($1)==1} {/showme {@Chr(10)@AnsiBold()@ForeYellow()Syntax: @ForeRed()values @ForeGreen()tradepost command @ForeYellow()- @ForeGreen()tradepost @ForeYellow()must be the first three or more letters and @ForeGreen()command @ForeYellow()can be anything.@AnsiReset()}} {/if {$1 == "list"} {/showme {@Chr(10)@AnsiBold()@ForeYellow()Medievia Trade Posts@AnsiReset()};/showme {@AnsiBold()@ForeYellow()--------------------};/loop {1,18} {/showme {@AnsiBold()@ForeGreen()@GetArray(shopnames,$LoopCount,0)@AnsiReset()}}} {/var {Shop} {@Word($1,1)};/var {NumWordLength} {@Len($Shop)};/if {$NumWordLength < 3} {/showme {@AnsiBold()@ForeCyan()Shop argument must be 3 letters or more long.@AnsiReset()}} {/if {@StrStr(@Lower(@GetArray(shopnames,1,0)),@Lower(@Word($1,1))) != -1} {/chatall @GetArray(shopnames, 1,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,1,1) yards] @GetArray(itstf,1,1): @GetArray(itstf,1,7). @GetArray(itstf,2,1): @GetArray(itstf,2,7). @GetArray(itstf,3,1): @GetArray(itstf,3,7). @GetArray(itstf,4,1): @GetArray(itstf,4,7). @GetArray(itstf,5,1): @GetArray(itstf,5,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,2,0)),@Lower(@Word($1,1))) != -1} {/chatall @GetArray(shopnames,2,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,2,1) yards] @GetArray(itstf,6,1): @GetArray(itstf,6,7). @GetArray(itstf,7,1): @GetArray(itstf,7,7). @GetArray(itstf,8,1): @GetArray(itstf,8,7)} {/if {@StrStr(@Lower(@GetArray(shopnames,3,0)),@Lower(@Word($1,1))) != -1} {/chatall @GetArray(shopnames,3,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,3,1) yards] @GetArray(itstf,9,1): @GetArray(itstf,9,7). @GetArray(itstf,10,1): @GetArray(itstf,10,7). @GetArray(itstf,11,1): @GetArray(itstf,11,7). @GetArray(itstf,12,1): @GetArray(itstf,12,7). @GetArray(itstf,13,1): @GetArray(itstf,13,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,4,0)),@Lower(@Word($1,1))) != -1} {/chatall @GetArray(shopnames,4,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,4,1) yards] @GetArray(itstf,14,1): @GetArray(itstf,14,7). @GetArray(itstf,15,1): @GetArray(itstf,15,7). @GetArray(itstf,16,1): @GetArray(itstf,16,7). @GetArray(itstf,17,1): @GetArray(itstf,17,7). @GetArray(itstf,18,1): @GetArray(itstf,18,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,5,0)),@Lower(@Word($1,1))) != -1} {/chatall @GetArray(shopnames,5,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,5,1) yards] @GetArray(itstf,19,1): @GetArray(itstf,19,7). @GetArray(itstf,20,1): @GetArray(itstf,20,7). @GetArray(itstf,21,1): @GetArray(itstf,21,7). @GetArray(itstf,22,1): @GetArray(itstf,22,7). @GetArray(itstf,23,1): @GetArray(itstf,23,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,6,0)),@Lower(@Word($1,1))) != -1} {/chatall @GetArray(shopnames,6,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,6,1) yards] @GetArray(itstf,24,1): @GetArray(itstf,24,7). @GetArray(itstf,25,1): @GetArray(itstf,25,7). @GetArray(itstf,26,1): @GetArray(itstf,26,7). @GetArray(itstf,27,1): @GetArray(itstf,27,7). @GetArray(itstf,28,1): @GetArray(itstf,28,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,7,0)),@Lower(@Word($1,1))) != -1} {/chatall @GetArray(shopnames,7,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,7,1) yards] @GetArray(itstf,29,1): @GetArray(itstf,29,7). @GetArray(itstf,30,1): @GetArray(itstf,30,7). @GetArray(itstf,31,1): @GetArray(itstf,31,7). @GetArray(itstf,32,1): @GetArray(itstf,32,7). @GetArray(itstf,33,1): @GetArray(itstf,33,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,8,0)),@Lower(@Word($1,1))) != -1} {/chatall @GetArray(shopnames,8,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,8,1) yards] @GetArray(itstf,34,1): @GetArray(itstf,34,7). @GetArray(itstf,35,1): @GetArray(itstf,35,7). @GetArray(itstf,36,1): @GetArray(itstf,36,7). @GetArray(itstf,37,1): @GetArray(itstf,37,7). @GetArray(itstf,38,1): @GetArray(itstf,38,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,9,0)),@Lower(@Word($1,1))) != -1} {/chatall @GetArray(shopnames,9,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,9,1) yards] @GetArray(itstf,39,1): @GetArray(itstf,39,7). @GetArray(itstf,40,1): @GetArray(itstf,40,7). @GetArray(itstf,41,1): @GetArray(itstf,41,7). @GetArray(itstf,42,1): @GetArray(itstf,42,7). @GetArray(itstf,43,1): @GetArray(itstf,43,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,10,0)),@Lower(@Word($1,1))) != -1} {/chatall @GetArray(shopname,10,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,10,1) yards] @GetArray(itstf,44,1): @GetArray(itstf,44,7). @GetArray(itstf,45,1): @GetArray(itstf,45,7). @GetArray(itstf,46,1): @GetArray(itstf,46,7). @GetArray(itstf,47,1): @GetArray(itstf,47,7). @GetArray(itstf,48,1): @GetArray(itstf,48,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,11,0)),@Lower(@Word($1,1))) != -1} {/chatall @GetArray(shopnames,11,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,11,1) yards] @GetArray(itstf,49,1): @GetArray(itstf,49,7). @GetArray(itstf,50,1): @GetArray(itstf,50,7). @GetArray(itstf,51,1): @GetArray(itstf,51,7). @GetArray(itstf,52,1): @GetArray(itstf,52,7). @GetArray(itstf,53,1): @GetArray(itstf,53,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,12,0)),@Lower(@Word($1,1))) != -1} {/chatall @GetArray(shopnames,12,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,12,1) yards] @GetArray(itstf,54,1): @GetArray(itstf,54,7). @GetArray(itstf,55,1): @GetArray(itstf,55,7). @GetArray(itstf,56,1): @GetArray(itstf,56,7). @GetArray(itstf,57,1): @GetArray(itstf,57,7). @GetArray(itstf,58,1): @GetArray(itstf,58,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,13,0)),@Lower(@Word($1,1))) != -1} {/chatall @GetArray(shopnames,13,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,13,1) yards] @GetArray(itstf,59,1): @GetArray(itstf,59,7). @GetArray(itstf,60,1): @GetArray(itstf,60,7). @GetArray(itstf,61,1): @GetArray(itstf,61,7). @GetArray(itstf,62,1): @GetArray(itstf,62,7). @GetArray(itstf,63,1): @GetArray(itstf,63,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,14,0)),@Lower(@Word($1,1))) != -1} {/chatall @GetArray(shopnames,14,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,14,1) yards] @GetArray(itstf,64,1): @GetArray(itstf,64,7). @GetArray(itstf,65,1): @GetArray(itstf,65,7). @GetArray(itstf,66,1): @GetArray(itstf,66,7). @GetArray(itstf,67,1): @GetArray(itstf,67,7). @GetArray(itstf,68,1): @GetArray(itstf,68,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,15,0)),@Lower(@Word($1,1))) != -1} {/chatall @GetArray(shopnames,15,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,15,1) yards] @GetArray(itstf,69,1): @GetArray(itstf,69,7). @GetArray(itstf,70,1): @GetArray(itstf,70,7). @GetArray(itstf,71,1): @GetArray(itstf,71,7). @GetArray(itstf,72,1): @GetArray(itstf,72,7). @GetArray(itstf,73,1): @GetArray(itstf,73,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,16,0)),@Lower(@Word($1,1))) != -1} {/chatall @GetArray(shopnames,16,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,16,1) yards] @GetArray(itstf,74,1): @GetArray(itstf,74,7). @GetArray(itstf,75,1): @GetArray(itstf,75,7). @GetArray(itstf,76,1): @GetArray(itstf,76,7). @GetArray(itstf,77,1): @GetArray(itstf,77,7). @GetArray(itstf,78,1): @GetArray(itstf,78,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,17,0)),@Lower(@Word($1,1))) != -1} {/chatall @GetArray(shopname,17,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,17,1) yards] @GetArray(itstf,79,1): @GetArray(itstf,79,7). @GetArray(itstf,80,1): @GetArray(itstf,80,7). @GetArray(itstf,81,1): @GetArray(itstf,81,7). @GetArray(itstf,82,1): @GetArray(itstf,82,7). @GetArray(itstf,83,1): @GetArray(itstf,83,7).} {/if {@StrStr(@Lower(@GetArray(shopnames,18,0)),@Lower(@Word($1,1))) != -1} {/chatall @GetArray(shopnames,18,0) To @GetArray(tpname,1,3) [@GetArray(itemmoves,18,1) yards] @GetArray(itstf,84,1): @GetArray(itstf,84,7). @GetArray(itstf,85,1): @GetArray(itstf,85,7). @GetArray(itstf,86,1): @GetArray(itstf,86,7). @GetArray(itstf,87,1): @GetArray(itstf,87,7). @GetArray(itstf,88,1): @GetArray(itstf,88,7).}}}}}}}}}}}}}}}}}}}}}} {TValias}

/alias {update} {/math {counter} {$counter + 1}} {TValias}

/alias {showprices %1} {/showme {@AnsiBold()@ForeYellow()@GetArray(tpname,1,1)};/showme {@AnsiBold()@ForeGreen()--------------------------------------------------------------------------------};/if {@IsEmpty($1)==1} {/var {MinPrice} {$tvMinPrice}} {/var {MinPrice} {$1}};/showme {@AnsiBold()@ForeRed()Trade Post              Item            Amount      Price Each    Yards   PPM};/showme {@AnsiBold()@ForeGreen()--------------------------------------------------------------------------------};/loop {1,88} {/if {@GetArray(itstf,$LoopCount,7) >= $MinPrice} {/if {@StrStr(@Lower(@Left(@GetArray(shopnames,@GetArray(itstf,$LoopCount,2),0),3)),@Lower(@GetArray(tpname,1,2))) == -1} {/math {ShopPad} {24 - @Len(@GetArray(shopnames,@GetArray(itstf,$LoopCount,2),0))};/math {ItemPad} {16 - @Len(@GetArray(itstf,$LoopCount,1))};/math {YardPad} {8 - @Len(@GetArray(itemmoves,@GetArray(itstf,$LoopCount,2),0))};/math {ProfitPad} {12 - @Len(@GetArray(itstf,$LoopCount,7))};/math {AmountPad} {14 - @Len(@GetArray(itstf,$LoopCount,5))};/showme {@AnsiBold()@ForeCyan()@PadRight(@GetArray(shopnames,@GetArray(itstf,$LoopCount,2),0),.,$ShopPad)@AnsiReset()@PadRight(@GetArray(itstf,$LoopCount,1),.,$ItemPad)@AnsiReset()@PadRight(@GetArray(itstf,$LoopCount,4),.,$ProfitPad)@AnsiBold()@ForeYellow()@PadRight(@GetArray(itstf,$LoopCount,5),.,$AmountPad)@AnsiReset()@PadRight(@GetArray(itemmoves,@GetArray(itstf,$LoopCount,2),0),.,$YardPad)@GetArray(itstf,$LoopCount,6)}} {}} {}}} {TValias}

/alias {showppm %1} {/showme {@AnsiBold()@ForeYellow()@GetArray(tpname,1,1)};/showme {@AnsiBold()@ForeGreen()--------------------------------------------------------------------------------};/if {@IsEmpty($1)==1} {/var {MinPPM} {$tvMinPPM}} {/var {MinPPM} {$1}};/showme {@AnsiBold()@ForeRed()Trade Post              Item            Amount      Price Each    Yards   PPM};/showme {@AnsiBold()@ForeGreen()--------------------------------------------------------------------------------};/loop {1,88} {/if {@StripAnsi(@GetArray(itstf,$LoopCount,6)) >= $MinPPM} {/if {@StrStr(@Lower(@Left(@GetArray(shopnames,@GetArray(itstf,$LoopCount,2),0),3)),@Lower(@GetArray(tpname,1,2))) == -1} {/math {ShopPad} {24 - @Len(@GetArray(shopnames,@GetArray(itstf,$LoopCount,2),0))};/math {ItemPad} {16 - @Len(@GetArray(itstf,$LoopCount,1))};/math {YardPad} {8 - @Len(@GetArray(itemmoves,@GetArray(itstf,$LoopCount,2),0))};/math {ProfitPad} {12 - @Len(@GetArray(itstf,$LoopCount,7))};/math {AmountPad} {14 - @Len(@GetArray(itstf,$LoopCount,5))};/showme {@AnsiBold()@ForeCyan()@PadRight(@GetArray(shopnames,@GetArray(itstf,$LoopCount,2),0),.,$ShopPad)@AnsiReset()@PadRight(@GetArray(itstf,$LoopCount,1),.,$ItemPad)@AnsiReset()@PadRight(@GetArray(itstf,$LoopCount,4),.,$ProfitPad)@AnsiBold()@ForeYellow()@PadRight(@GetArray(itstf,$LoopCount,5),.,$AmountPad)@AnsiReset()@PadRight(@GetArray(itemmoves,@GetArray(itstf,$LoopCount,2),0),.,$YardPad)@GetArray(itstf,$LoopCount,6)}} {}} {}}} {TValias}

/alias {showpp %1} {/showme {@AnsiBold()@ForeYellow()@GetArray(tpname,1,1)};/showme {@AnsiBold()@ForeGreen()--------------------------------------------------------------------------------};/if {@IsEmpty($1)==1} {/var {MinPPM} {$tvMinPPM};/var {MinPrice} {$tvMinPrice}} {/var {MinPPM} {@Word($1,2)};/if {@IsEmpty($MinPPM)} {/var {MinPPM} {$tvMinPPM}} {};/var {MinPrice} {@Word($1,1)}};/showme {@AnsiBold()@ForeRed()Trade Post              Item            Amount      Price Each    Yards   PPM};/showme {@AnsiBold()@ForeGreen()--------------------------------------------------------------------------------};/loop {1,88} {/if {@StripAnsi(@GetArray(itstf,$LoopCount,6)) >= $MinPPM && @GetArray(itstf,$LoopCount,7) >= $MinPrice} {/if {@StrStr(@Lower(@Left(@GetArray(shopnames,@GetArray(itstf,$LoopCount,2),0),3)),@Lower(@GetArray(tpname,1,2))) == -1} {/math {ShopPad} {24 - @Len(@GetArray(shopnames,@GetArray(itstf,$LoopCount,2),0))};/math {ItemPad} {16 - @Len(@GetArray(itstf,$LoopCount,1))};/math {YardPad} {8 - @Len(@GetArray(itemmoves,@GetArray(itstf,$LoopCount,2),0))};/math {ProfitPad} {12 - @Len(@GetArray(itstf,$LoopCount,7))};/math {AmountPad} {14 - @Len(@GetArray(itstf,$LoopCount,5))};/showme {@AnsiBold()@ForeCyan()@PadRight(@GetArray(shopnames,@GetArray(itstf,$LoopCount,2),0),.,$ShopPad)@AnsiReset()@PadRight(@GetArray(itstf,$LoopCount,1),.,$ItemPad)@AnsiReset()@PadRight(@GetArray(itstf,$LoopCount,4),.,$ProfitPad)@AnsiBold()@ForeYellow()@PadRight(@GetArray(itstf,$LoopCount,5),.,$AmountPad)@AnsiReset()@PadRight(@GetArray(itemmoves,@GetArray(itstf,$LoopCount,2),0),.,$YardPad)@GetArray(itstf,$LoopCount,6)}} {}} {}}} {TValias}

/action {You are handed 20000 gold and some men take a large packhorse away.} {/disablegroup {TVtriggers};/disablegroup {TVgag};/var {counter} {1}} {TVtriggers}
/action {They dont buy or sell that here.} {/assign {itstf} {$counter,4} {@ForeGreen()0@AnsiReset()};/assign {itstf} {$counter,7} {0};/assign {itstf} {$counter,5} {0};/if {@GetArray(tpmoves,@GetArray(itstf,$counter,2),0) >=0)} {/assign {itstf} {$counter,6} {@ForeGreen()0@AnsiReset()}} {/assign {itstf} {$counter,6} {@ForeGreen()NA@AnsiReset()}};update} {TVtriggers}
/action {We will pay you %2 gold for each %3.} {/var {mult1} {$2};/assign {itstf} {$counter,5} {$2};/var {mult2} {@GetArray(itstf,$counter,3)};/math {profit} {$mult1 * $mult2};/assign {itstf} {$counter,7} {@Var(profit)};/if {$profit >=6000000} {/assign {itstf} {$counter,4} {@AnsiBold()@ForeRed()@Var(profit)@AnsiReset()}} {/if {$profit >= 5000000} {/assign {itstf} {$counter,4} {@AnsiBold()@ForeYellow()@Var(profit)@AnsiReset()}} {/if {$profit >= 4000000} {/assign {itstf} {$counter,4} {@AnsiBold()@ForeBlue()@Var(profit)@AnsiReset()}} {/if {$profit >= 3000000} {/assign {itstf} {$counter,4} {@AnsiBold()@ForeMagenta()@Var(profit)@AnsiReset()}} {/if {$profit >= 2000000} {/assign {itstf} {$counter,4} {@AnsiBold()@ForeGreen()@Var(profit)@AnsiReset()}} {/if {$profit >= 1000000} {/assign {itstf} {$counter,4} {@AnsiBold()@ForeCyan()@Var(profit)@AnsiReset()}} {/assign {itstf} {$counter,4} {@AnsiBold()@ForeWhite()@Var(profit)@AnsiReset()}}}}}}};/if {@GetArray(itemmoves,@GetArray(itstf,$counter,2),0) >= 0} {/var {moves} {@GetArray(itemmoves,@GetArray(itstf,$counter,2),0)};/math {PPM} {$profit / $moves};/if {$PPM >= 750} {/assign {itstf} {$counter,6} {@AnsiBold()@ForeRed()@Var(PPM)@AnsiReset()}} {/if {$PPM >= 650} {/assign {itstf} {$counter,6} {@AnsiBold()@ForeYellow()@Var(PPM)@AnsiReset()}} {/if {$PPM >= 550} {/assign {itstf} {$counter,6} {@AnsiBold()@ForeGreen()@Var(PPM)@AnsiReset()}} {/if {$PPM >= 450} {/assign {itstf} {$counter,6} {@AnsiBold()@ForeCyan()@Var(PPM)@AnsiReset()}} {/assign {itstf} {$counter,6} {@AnsiBold()@ForeWhite()@Var(PPM)@AnsiReset()}}}}}} {/assign {itstf} {$counter,6} {@ForeGreen()NA@AnsiReset()}};update} {TVtriggers}

/disablegroup {TVtriggers}
/disablegroup {TVgag}
/disablegroup {TVtrigs}

/action {A Minotaur Trading Outpost} {/loop {1,18} {/assign {itemmoves} {$LoopCount} {@GetArray(naemoves,$LoopCount,0)}};/assign {tpname} {1,1} {A Minotaur Trading Outpost [NaeraMae]};/assign {tpname} {1,2} {nae};/assign {tpname} {1,3} {NaerMae};/disablegroup {TVPosts}} {TVPosts}
/action {A Quiet Temple Shop} {/loop {1,18} {/assign {itemmoves} {$LoopCount} {@GetArray(elwmoves,$LoopCount,0)}};/assign {tpname} {1,1} {A Quiet, Temple Shop [Elwyn]};/assign {tpname} {1,2} {elw};/assign {tpname} {1,3} {Elwynn};/disablegroup {TVPosts}} {TVPosts}
/action {A Ranger's Cabin} {/loop {1,18} {/assign {itemmoves} {$LoopCount} {@GetArray(ranmoves,$LoopCount,0)}};/assign {tpname} {1,1} {A Ranger's Cabin};/assign {tpname} {1,2} {ran};/assign {tpname} {1,3} {Ranger's Cabin};/disablegroup {TVPosts}} {TVPosts}
/action {The Hidden City Tradeshop} {/loop {1,18} {/assign {itemmoves} {$LoopCount} {@GetArray(hidmoves,$LoopCount,0)}};/assign {tpname} {1,1} {The Hidden City Tradeshop [ENatDae]};/assign {tpname} {1,2} {ena};/assign {tpname} {1,3} {EnatDae};/disablegroup {TVPosts}} {TVPosts}
/action {An Elven Marketplace} {/loop {1,18} {/assign {itemmoves} {$LoopCount} {@GetArray(athmoves,$LoopCount,0)}};/assign {tpname} {1,1} {An Elven Marketplace [City of Athelasea]};/assign {tpname} {1,2} {ath};/assign {tpname} {1,3} {Athelasea};/disablegroup {TVPosts}} {TVPosts}
/action {DeRah Villadom's Small Trading Post} {/loop {1,18} {/assign {itemmoves} {$LoopCount} {@GetArray(dermoves,$LoopCount,0)}};/assign {tpname} {1,1} {DeRah Villadom's Small Trading Post};/assign {tpname} {1,2} {der};/assign {tpname} {1,3} {Derah};/disablegroup {TVPosts}} {TVPosts}
/action {Hidden Valley Traders, Inc.} {/loop {1,18} {/assign {itemmoves} {$LoopCount} {@GetArray(ruemoves,$LoopCount,0)}};/assign {tpname} {1,1} {Hidden Valley Traders, Inc. [Ruellia]};/assign {tpname} {1,2} {rue};/assign {tpname} {1,3} {Ruellia};/disablegroup {TVPosts}} {TVPosts}
/action {Medievia Trading Shop} {/loop {1,18} {/assign {itemmoves} {$LoopCount} {@GetArray(medmoves,$LoopCount,0)}};/assign {tpname} {1,1} {Medievia Trading Shop};/assign {tpname} {1,2} {med};/assign {tpname} {1,3} {Medievia};/disablegroup {TVPosts}} {TVPosts}
/action {New Ashton Trading Post} {/loop {1,18} {/assign {itemmoves} {$LoopCount} {@GetArray(ashmoves,$LoopCount,0)}};/assign {tpname} {1,1} {New Ashton Trading Post};/assign {tpname} {1,2} {ash};/assign {tpname} {1,3} {New Ashton};/disablegroup {TVPosts}} {TVPosts}
/action {Riverton Trading Partners} {/loop {1,18} {/assign {itemmoves} {$LoopCount} {@GetArray(rivmoves,$LoopCount,0)}};/assign {tpname} {1,1} {Riverton Trading Partners};/assign {tpname} {1,2} {riv};/assign {tpname} {1,3} {Riverton};/disablegroup {TVPosts}} {TVPosts}
/action {Sea's End Direct Merchants} {/loop {1,18} {/assign {itemmoves} {$LoopCount} {@GetArray(endmoves,$LoopCount,0)}};/assign {tpname} {1,1} {Sea's End Direct Merchants};/assign {tpname} {1,2} {end};/assign {tpname} {1,3} {Sea's End};/disablegroup {TVPosts}} {TVPosts}
/action {Tanivsport Traders} {/loop {1,18} {/assign {itemmoves} {$LoopCount} {@GetArray(mysmoves,$LoopCount,0)}};/assign {tpname} {1,1} {Tanivsport Traders [Mystara]};/assign {tpname} {1,2} {mys};/assign {tpname} {1,3} {Mystarra};/disablegroup {TVPosts}} {TVPosts}
/action {The Lizard Boulangere} {/loop {1,18} {/assign {itemmoves} {$LoopCount} {@GetArray(lyrmoves,$LoopCount,0)}};/assign {tpname} {1,1} {The Lizard Boulangere [Lyranoth]};/assign {tpname} {1,2} {lyr};/assign {tpname} {1,3} {Lyranoth};/disablegroup {TVPosts}} {TVPosts}
/action {The Trading Post of G'dangus} {/loop {1,18} {/assign {itemmoves} {$LoopCount} {@GetArray(gdamoves,$LoopCount,0)}};/assign {tpname} {1,1} {The Trading Post of G'dangus};/assign {tpname} {1,2} {gda};/assign {tpname} {1,3} {Gdangus};/disablegroup {TVPosts}} {TVPosts}
/action {Trading Post of the City of Karlisna} {/loop {1,18} {/assign {itemmoves} {$LoopCount} {@GetArray(karmoves,$LoopCount,0)}};/assign {tpname} {1,1} {Trading Post of the City of Karlisna};/assign {tpname} {1,2} {kar};/assign {tpname} {1,3} {Karlisna};/disablegroup {TVPosts}} {TVPosts}
/action {Trading Post of the Dark Army} {/loop {1,18} {/assign {itemmoves} {$LoopCount} {@GetArray(genmoves,$LoopCount,0)}};/assign {tpname} {1,1} {Trading Post of the Dark Army [New Genesia]};/assign {tpname} {1,2} {gen};/assign {tpname} {1,3} {New Genesia};/disablegroup {TVPosts}} {TVPosts}
/action {Trading Shop of Trellor City} {/loop {1,18} {/assign {itemmoves} {$LoopCount} {@GetArray(tremoves,$LoopCount,0)}};/assign {tpname} {1,1} {Trading Shop of Trellor City};/assign {tpname} {1,2} {tre};/assign {tpname} {1,3} {Trellor};/disablegroup {TVPosts}} {TVPosts}
/action {Ur-vile Feeding Hall} {/loop {1,18} {/assign {itemmoves} {$LoopCount} {@GetArray(urvmoves,$LoopCount,0)}};/assign {tpname} {1,1} {Ur-vile Feeding Hall [Temple of Thanos]};/assign {tpname} {1,2} {tha};/assign {tpname} {1,3} {Ur-vile};/disablegroup {TVPosts}} {TVPosts}
/action {Vanlarra Imports and Exports} {/loop {1,18} {/assign {itemmoves} {$LoopCount} {@GetArray(vanmoves,$LoopCount,0)}};/assign {tpname} {1,1} {Vanlarra Imports and Exports};/assign {tpname} {1,2} {van};/assign {tpname} {1,3} {Vanlarra};/disablegroup {TVPosts}} {TVPosts}
/disablegroup {TVPosts}

/array {tpname} {1,3} {TVVariables}
/array {itemmoves} {18} {TVVariables}

/var {tvMinPrice} {1000000} {TVVariables}
/var {tvMinPPM} {450} {TVVariables}

/array {shopnames} {18} {TVVariables}
/assign {shopnames} {1} {DeRah}
/assign {shopnames} {2} {Elwyn}
/assign {shopnames} {3} {Gdangus}
/assign {shopnames} {4} {Lyryanoth}
/assign {shopnames} {5} {NaeraMae}
/assign {shopnames} {6} {Medievia}
/assign {shopnames} {7} {Mystara}
/assign {shopnames} {8} {New Ashton}
/assign {shopnames} {9} {New Genesia}
/assign {shopnames} {10} {Ranger's Cabin}
/assign {shopnames} {11} {Riverton}
/assign {shopnames} {12} {Ruellia}
/assign {shopnames} {13} {Sea's End}
/assign {shopnames} {14} {Vanlarra}
/assign {shopnames} {15} {Karlisna}
/assign {shopnames} {16} {Trellor}
/assign {shopnames} {17} {Athelasea}
/assign {shopnames} {18} {EnatDae}
 
/array {ashmoves} {18} {TVVariables}
/assign {ashmoves} {1} {1785}
/assign {ashmoves} {2} {1307}
/assign {ashmoves} {3} {1666}
/assign {ashmoves} {4} {1892}
/assign {ashmoves} {5} {1816}
/assign {ashmoves} {6} {781}
/assign {ashmoves} {7} {1336}
/assign {ashmoves} {8} {100000}
/assign {ashmoves} {9} {1196}
/assign {ashmoves} {10} {1063}
/assign {ashmoves} {11} {1333}
/assign {ashmoves} {12} {1537}
/assign {ashmoves} {13} {1952}
/assign {ashmoves} {14} {1547}
/assign {ashmoves} {15} {752}
/assign {ashmoves} {16} {494}
/assign {ashmoves} {17} {668}
/assign {ashmoves} {18} {1021}

/array {dermoves} {18} {TVVariables}
/assign {dermoves} {1} {100000}
/assign {dermoves} {2} {1295}
/assign {dermoves} {3} {2376}
/assign {dermoves} {4} {1880}
/assign {dermoves} {5} {2769}
/assign {dermoves} {6} {1072}
/assign {dermoves} {7} {1627}
/assign {dermoves} {8} {1785}
/assign {dermoves} {9} {1571}
/assign {dermoves} {10} {1348}
/assign {dermoves} {11} {1642}
/assign {dermoves} {12} {1912}
/assign {dermoves} {13} {2662}
/assign {dermoves} {14} {1838}
/assign {dermoves} {15} {2292}
/assign {dermoves} {16} {2264}
/assign {dermoves} {17} {1225}
/assign {dermoves} {18} {862}

/array {elwmoves} {18} {TVVariables}
/assign {elwmoves} {1} {1295}
/assign {elwmoves} {2} {100000}
/assign {elwmoves} {3} {1898}
/assign {elwmoves} {4} {655}
/assign {elwmoves} {5} {2281}
/assign {elwmoves} {6} {594}
/assign {elwmoves} {7} {1149}
/assign {elwmoves} {8} {1307}
/assign {elwmoves} {9} {1093}
/assign {elwmoves} {10} {870}
/assign {elwmoves} {11} {1146}
/assign {elwmoves} {12} {1434}
/assign {elwmoves} {13} {2184}
/assign {elwmoves} {14} {1336}
/assign {elwmoves} {15} {1814}
/assign {elwmoves} {16} {1786}
/assign {elwmoves} {17} {747}
/assign {elwmoves} {18} {418}

/array {endmoves} {18} {TVVariables}
/assign {endmoves} {1} {2662}
/assign {endmoves} {2} {2184}
/assign {endmoves} {3} {672}
/assign {endmoves} {4} {2768}
/assign {endmoves} {5} {2020}
/assign {endmoves} {6} {1616}
/assign {endmoves} {7} {1666}
/assign {endmoves} {8} {1952}
/assign {endmoves} {9} {2253}
/assign {endmoves} {10} {1347}
/assign {endmoves} {11} {1483}
/assign {endmoves} {12} {2594}
/assign {endmoves} {13} {100000}
/assign {endmoves} {14} {1709}
/assign {endmoves} {15} {1149}
/assign {endmoves} {16} {1774}
/assign {endmoves} {17} {1548}
/assign {endmoves} {18} {1898}

/array {gdamoves} {18} {TVVariables}
/assign {gdamoves} {1} {2376}
/assign {gdamoves} {2} {1898}
/assign {gdamoves} {3} {100000}
/assign {gdamoves} {4} {2483}
/assign {gdamoves} {5} {1638}
/assign {gdamoves} {6} {1330}
/assign {gdamoves} {7} {1380}
/assign {gdamoves} {8} {1666}
/assign {gdamoves} {9} {1967}
/assign {gdamoves} {10} {1061}
/assign {gdamoves} {11} {1197}
/assign {gdamoves} {12} {2308}
/assign {gdamoves} {13} {672}
/assign {gdamoves} {14} {1423}
/assign {gdamoves} {15} {863}
/assign {gdamoves} {16} {1562}
/assign {gdamoves} {17} {1262}
/assign {gdamoves} {18} {1612}

/array {genmoves} {18} {TVVariables}
/assign {genmoves} {1} {1571}
/assign {genmoves} {2} {1093}
/assign {genmoves} {3} {1967}
/assign {genmoves} {4} {1678}
/assign {genmoves} {5} {2262}
/assign {genmoves} {6} {657}
/assign {genmoves} {7} {1212}
/assign {genmoves} {8} {1196}
/assign {genmoves} {9} {100000}
/assign {genmoves} {10} {939}
/assign {genmoves} {11} {1209}
/assign {genmoves} {12} {399}
/assign {genmoves} {13} {2253}
/assign {genmoves} {14} {1423}
/assign {genmoves} {15} {1795}
/assign {genmoves} {16} {1500}
/assign {genmoves} {17} {770}
/assign {genmoves} {18} {807}

/array {lyrmoves} {18} {TVVariables}
/assign {lyrmoves} {1} {1880}
/assign {lyrmoves} {2} {655}
/assign {lyrmoves} {3} {2183}
/assign {lyrmoves} {4} {100000}
/assign {lyrmoves} {5} {2865}
/assign {lyrmoves} {6} {1179}
/assign {lyrmoves} {7} {1734}
/assign {lyrmoves} {8} {1892}
/assign {lyrmoves} {9} {1678}
/assign {lyrmoves} {10} {1455}
/assign {lyrmoves} {11} {1731}
/assign {lyrmoves} {12} {2019}
/assign {lyrmoves} {13} {2768}
/assign {lyrmoves} {14} {1921}
/assign {lyrmoves} {15} {2399}
/assign {lyrmoves} {16} {2371}
/assign {lyrmoves} {17} {1332}
/assign {lyrmoves} {18} {1003}

/array {medmoves} {18} {TVVariables}
/assign {medmoves} {1} {1072}
/assign {medmoves} {2} {594}
/assign {medmoves} {3} {1330}
/assign {medmoves} {4} {1179}
/assign {medmoves} {5} {1710}
/assign {medmoves} {6} {100000}
/assign {medmoves} {7} {575}
/assign {medmoves} {8} {781}
/assign {medmoves} {9} {657}
/assign {medmoves} {10} {302}
/assign {medmoves} {11} {572}
/assign {medmoves} {12} {998}
/assign {medmoves} {13} {1616}
/assign {medmoves} {14} {786}
/assign {medmoves} {15} {1246}
/assign {medmoves} {16} {1269}
/assign {medmoves} {17} {221}
/assign {medmoves} {18} {308}

/array {mysmoves} {18} {TVVariables}
/assign {mysmoves} {1} {1627}
/assign {mysmoves} {2} {1149}
/assign {mysmoves} {3} {1380}
/assign {mysmoves} {4} {1734}
/assign {mysmoves} {5} {2120}
/assign {mysmoves} {6} {575}
/assign {mysmoves} {7} {100000}
/assign {mysmoves} {8} {1336}
/assign {mysmoves} {9} {1212}
/assign {mysmoves} {10} {743}
/assign {mysmoves} {11} {307}
/assign {mysmoves} {12} {1553}
/assign {mysmoves} {13} {1666}
/assign {mysmoves} {14} {281}
/assign {mysmoves} {15} {1256}
/assign {mysmoves} {16} {1824}
/assign {mysmoves} {17} {776}
/assign {mysmoves} {18} {863}

/array {naemoves} {18} {TVVariables}
/assign {naemoves} {1} {2759}
/assign {naemoves} {2} {2281}
/assign {naemoves} {3} {1734}
/assign {naemoves} {4} {2865}
/assign {naemoves} {5} {100000}
/assign {naemoves} {6} {1710}
/assign {naemoves} {7} {2120}
/assign {naemoves} {8} {1219}
/assign {naemoves} {9} {2262}
/assign {naemoves} {10} {1847}
/assign {naemoves} {11} {1983}
/assign {naemoves} {12} {2603}
/assign {naemoves} {13} {2020}
/assign {naemoves} {14} {2209}
/assign {naemoves} {15} {1638}
/assign {naemoves} {16} {1816}
/assign {naemoves} {17} {1643}
/assign {naemoves} {18} {1995}

/array {ranmoves} {18} {TVVariables}
/assign {ranmoves} {1} {1348}
/assign {ranmoves} {2} {870}
/assign {ranmoves} {3} {1061}
/assign {ranmoves} {4} {1455}
/assign {ranmoves} {5} {1847}
/assign {ranmoves} {6} {302}
/assign {ranmoves} {7} {743}
/assign {ranmoves} {8} {1063}
/assign {ranmoves} {9} {939}
/assign {ranmoves} {10} {100000}
/assign {ranmoves} {11} {740}
/assign {ranmoves} {12} {1280}
/assign {ranmoves} {13} {1347}
/assign {ranmoves} {14} {958}
/assign {ranmoves} {15} {976}
/assign {ranmoves} {16} {1551}
/assign {ranmoves} {17} {503}
/assign {ranmoves} {18} {584}

/array {rivmoves} {18} {TVVariables}
/assign {rivmoves} {1} {1624}
/assign {rivmoves} {2} {1146}
/assign {rivmoves} {3} {1197}
/assign {rivmoves} {4} {1731}
/assign {rivmoves} {5} {1883}
/assign {rivmoves} {6} {572}
/assign {rivmoves} {7} {307}
/assign {rivmoves} {8} {1333}
/assign {rivmoves} {9} {1209}
/assign {rivmoves} {10} {740}
/assign {rivmoves} {11} {100000}
/assign {rivmoves} {12} {1550}
/assign {rivmoves} {13} {1483}
/assign {rivmoves} {14} {518}
/assign {rivmoves} {15} {1112}
/assign {rivmoves} {16} {1821}
/assign {rivmoves} {17} {773}
/assign {rivmoves} {18} {860}

/array {ruemoves} {18} {TVVariables}
/assign {ruemoves} {1} {1912}
/assign {ruemoves} {2} {1434}
/assign {ruemoves} {3} {2308}
/assign {ruemoves} {4} {2019}
/assign {ruemoves} {5} {2603}
/assign {ruemoves} {6} {998}
/assign {ruemoves} {7} {1553}
/assign {ruemoves} {8} {1537}
/assign {ruemoves} {9} {399}
/assign {ruemoves} {10} {1280}
/assign {ruemoves} {11} {1550}
/assign {ruemoves} {12} {100000}
/assign {ruemoves} {13} {2594}
/assign {ruemoves} {14} {1764}
/assign {ruemoves} {15} {2136}
/assign {ruemoves} {16} {2169}
/assign {ruemoves} {17} {1111}
/assign {ruemoves} {18} {1148}

/array {karmoves} {18} {TVVariables}
/assign {karmoves} {1} {2292}
/assign {karmoves} {2} {1814}
/assign {karmoves} {3} {863}
/assign {karmoves} {4} {2399}
/assign {karmoves} {5} {1638}
/assign {karmoves} {6} {1246}
/assign {karmoves} {7} {1256}
/assign {karmoves} {8} {752}
/assign {karmoves} {9} {1795}
/assign {karmoves} {10} {976}
/assign {karmoves} {11} {1112}
/assign {karmoves} {12} {2136}
/assign {karmoves} {13} {1149}
/assign {karmoves} {14} {1338}
/assign {karmoves} {15} {100000}
/assign {karmoves} {16} {1349}
/assign {karmoves} {17} {1176}
/assign {karmoves} {18} {1528}

/array {tremoves} {18} {TVVariables}
/assign {tremoves} {1} {2264}
/assign {tremoves} {2} {1786}
/assign {tremoves} {3} {1562}
/assign {tremoves} {4} {2371}
/assign {tremoves} {5} {1816}
/assign {tremoves} {6} {1269}
/assign {tremoves} {7} {1824}
/assign {tremoves} {8} {494}
/assign {tremoves} {9} {1500}
/assign {tremoves} {10} {1551}
/assign {tremoves} {11} {1821}
/assign {tremoves} {12} {2169}
/assign {tremoves} {13} {1774}
/assign {tremoves} {14} {2035}
/assign {tremoves} {15} {1349}
/assign {tremoves} {16} {100000}
/assign {tremoves} {17} {1160}
/assign {tremoves} {18} {1500}

/array {athmoves} {18} {TVVariables}
/assign {athmoves} {1} {1225}
/assign {athmoves} {2} {747}
/assign {athmoves} {3} {1262}
/assign {athmoves} {4} {1332}
/assign {athmoves} {5} {1643}
/assign {athmoves} {6} {221}
/assign {athmoves} {7} {776}
/assign {athmoves} {8} {668}
/assign {athmoves} {9} {770}
/assign {athmoves} {10} {503}
/assign {athmoves} {11} {773}
/assign {athmoves} {12} {1111}
/assign {athmoves} {13} {1548}
/assign {athmoves} {14} {987}
/assign {athmoves} {15} {1176}
/assign {athmoves} {16} {1160}
/assign {athmoves} {17} {100000}
/assign {athmoves} {18} {461}

/array {urvmoves} {18} {TVVariables}
/assign {urvmoves} {1} {1912}
/assign {urvmoves} {2} {1434}
/assign {urvmoves} {3} {2136}
/assign {urvmoves} {4} {2019}
/assign {urvmoves} {5} {3923}
/assign {urvmoves} {6} {998}
/assign {urvmoves} {7} {1553}
/assign {urvmoves} {8} {2169}
/assign {urvmoves} {9} {399}
/assign {urvmoves} {10} {1280}
/assign {urvmoves} {11} {1550}
/assign {urvmoves} {12} {1320}
/assign {urvmoves} {13} {2594}
/assign {urvmoves} {14} {1764}
/assign {urvmoves} {15} {2136}
/assign {urvmoves} {16} {2169}
/assign {urvmoves} {17} {1111}
/assign {urvmoves} {18} {1148}

/array {vanmoves} {18} {TVVariables}
/assign {vanmoves} {1} {1838}
/assign {vanmoves} {2} {1336}
/assign {vanmoves} {3} {1423}
/assign {vanmoves} {4} {1921}
/assign {vanmoves} {5} {2209}
/assign {vanmoves} {6} {786}
/assign {vanmoves} {7} {281}
/assign {vanmoves} {8} {1547}
/assign {vanmoves} {9} {1423}
/assign {vanmoves} {10} {958}
/assign {vanmoves} {11} {518}
/assign {vanmoves} {12} {1764}
/assign {vanmoves} {13} {1709}
/assign {vanmoves} {14} {100000}
/assign {vanmoves} {15} {1338}
/assign {vanmoves} {16} {2035}
/assign {vanmoves} {17} {987}
/assign {vanmoves} {18} {1059}

/array {hidmoves} {18} {TVVariables}
/assign {hidmoves} {1} {862}
/assign {hidmoves} {2} {418}
/assign {hidmoves} {3} {1612}
/assign {hidmoves} {4} {1003}
/assign {hidmoves} {5} {1995}
/assign {hidmoves} {6} {308}
/assign {hidmoves} {7} {863}
/assign {hidmoves} {8} {1021}
/assign {hidmoves} {9} {807}
/assign {hidmoves} {10} {584}
/assign {hidmoves} {11} {860}
/assign {hidmoves} {12} {1148}
/assign {hidmoves} {13} {1898}
/assign {hidmoves} {14} {1059}
/assign {hidmoves} {15} {1528}
/assign {hidmoves} {16} {1500}
/assign {hidmoves} {17} {461}
/assign {hidmoves} {18} {100000}

#Item Names
/array {itstf} {88,7} {TVVariables}
/assign {itstf} {1,1} {artifacts}
/assign {itstf} {2,1} {candles}
/assign {itstf} {3,1} {hemp}
/assign {itstf} {4,1} {herbs}
/assign {itstf} {5,1} {nuts}
/assign {itstf} {6,1} {aloe}
/assign {itstf} {7,1} {balm}
/assign {itstf} {8,1} {quills}
/assign {itstf} {9,1} {bananas}
/assign {itstf} {10,1} {beans}
/assign {itstf} {11,1} {rice}
/assign {itstf} {12,1} {seed}
/assign {itstf} {13,1} {tobacco}
/assign {itstf} {14,1} {eggs}
/assign {itstf} {15,1} {glowworms}
/assign {itstf} {16,1} {hides}
/assign {itstf} {17,1} {mushrooms}
/assign {itstf} {18,1} {venom}
/assign {itstf} {19,1} {armor}
/assign {itstf} {20,1} {chains}
/assign {itstf} {21,1} {mead}
/assign {itstf} {22,1} {pelts}
/assign {itstf} {23,1} {weapons}
/assign {itstf} {24,1} {ale}
/assign {itstf} {25,1} {raisins}
/assign {itstf} {26,1} {salt}
/assign {itstf} {27,1} {sugar}
/assign {itstf} {28,1} {tar}
/assign {itstf} {29,1} {carpet}
/assign {itstf} {30,1} {fish}
/assign {itstf} {31,1} {maps}
/assign {itstf} {32,1} {pearls}
/assign {itstf} {33,1} {teak}
/assign {itstf} {34,1} {copper}
/assign {itstf} {35,1} {jewels}
/assign {itstf} {36,1} {nails}
/assign {itstf} {37,1} {silicate}
/assign {itstf} {38,1} {silver}
/assign {itstf} {39,1} {adamantite}
/assign {itstf} {40,1} {coal}
/assign {itstf} {41,1} {iron}
/assign {itstf} {42,1} {poppyseed}
/assign {itstf} {43,1} {tools}
/assign {itstf} {44,1} {furs}
/assign {itstf} {45,1} {ivory}
/assign {itstf} {46,1} {rations}
/assign {itstf} {47,1} {sap}
/assign {itstf} {48,1} {timber}
/assign {itstf} {49,1} {brandy}
/assign {itstf} {50,1} {fertilizer}
/assign {itstf} {51,1} {grapes}
/assign {itstf} {52,1} {honey}
/assign {itstf} {53,1} {metals}
/assign {itstf} {54,1} {cloaks}
/assign {itstf} {55,1} {emeralds}
/assign {itstf} {56,1} {rope}
/assign {itstf} {57,1} {waybread}
/assign {itstf} {58,1} {willowbark}
/assign {itstf} {59,1} {ambergris}
/assign {itstf} {60,1} {kelp}
/assign {itstf} {61,1} {nets}
/assign {itstf} {62,1} {opals}
/assign {itstf} {63,1} {shells}
/assign {itstf} {64,1} {crystal}
/assign {itstf} {65,1} {figs}
/assign {itstf} {66,1} {molasses}
/assign {itstf} {67,1} {rum}
/assign {itstf} {68,1} {wands}
/assign {itstf} {69,1} {beer}
/assign {itstf} {70,1} {books}
/assign {itstf} {71,1} {cloth}
/assign {itstf} {72,1} {dates}
/assign {itstf} {73,1} {spices}
/assign {itstf} {74,1} {incense}
/assign {itstf} {75,1} {ink}
/assign {itstf} {76,1} {oils}
/assign {itstf} {77,1} {perfume}
/assign {itstf} {78,1} {wine}
/assign {itstf} {79,1} {arrows}
/assign {itstf} {80,1} {art}
/assign {itstf} {81,1} {bows}
/assign {itstf} {82,1} {flowers}
/assign {itstf} {83,1} {parchment}
/assign {itstf} {84,1} {quilts}
/assign {itstf} {85,1} {silk}
/assign {itstf} {86,1} {fans}
/assign {itstf} {87,1} {silkworms}
/assign {itstf} {88,1} {illutian}

#Item Indices
/assign {itstf} {1,2} {1}
/assign {itstf} {2,2} {1}
/assign {itstf} {3,2} {1}
/assign {itstf} {4,2} {1}
/assign {itstf} {5,2} {1}
/assign {itstf} {6,2} {2}
/assign {itstf} {7,2} {2}
/assign {itstf} {8,2} {2}
/assign {itstf} {9,2} {3}
/assign {itstf} {10,2} {3}
/assign {itstf} {11,2} {3}
/assign {itstf} {12,2} {3}
/assign {itstf} {13,2} {3}
/assign {itstf} {14,2} {4}
/assign {itstf} {15,2} {4}
/assign {itstf} {16,2} {4}
/assign {itstf} {17,2} {4}
/assign {itstf} {18,2} {4}
/assign {itstf} {19,2} {5}
/assign {itstf} {20,2} {5}
/assign {itstf} {21,2} {5}
/assign {itstf} {22,2} {5}
/assign {itstf} {23,2} {5}
/assign {itstf} {24,2} {6}
/assign {itstf} {25,2} {6}
/assign {itstf} {26,2} {6}
/assign {itstf} {27,2} {6}
/assign {itstf} {28,2} {6}
/assign {itstf} {29,2} {7}
/assign {itstf} {30,2} {7}
/assign {itstf} {31,2} {7}
/assign {itstf} {32,2} {7}
/assign {itstf} {33,2} {7}
/assign {itstf} {34,2} {8}
/assign {itstf} {35,2} {8}
/assign {itstf} {36,2} {8}
/assign {itstf} {37,2} {8}
/assign {itstf} {38,2} {8}
/assign {itstf} {39,2} {9}
/assign {itstf} {40,2} {9}
/assign {itstf} {41,2} {9}
/assign {itstf} {42,2} {9}
/assign {itstf} {43,2} {9}
/assign {itstf} {44,2} {10}
/assign {itstf} {45,2} {10}
/assign {itstf} {46,2} {10}
/assign {itstf} {47,2} {10}
/assign {itstf} {48,2} {10}
/assign {itstf} {49,2} {11}
/assign {itstf} {50,2} {11}
/assign {itstf} {51,2} {11}
/assign {itstf} {52,2} {11}
/assign {itstf} {53,2} {11}
/assign {itstf} {54,2} {12}
/assign {itstf} {55,2} {12}
/assign {itstf} {56,2} {12}
/assign {itstf} {57,2} {12}
/assign {itstf} {58,2} {12}
/assign {itstf} {59,2} {13}
/assign {itstf} {60,2} {13}
/assign {itstf} {61,2} {13}
/assign {itstf} {62,2} {13}
/assign {itstf} {63,2} {13}
/assign {itstf} {64,2} {14}
/assign {itstf} {65,2} {14}
/assign {itstf} {66,2} {14}
/assign {itstf} {67,2} {14}
/assign {itstf} {68,2} {14}
/assign {itstf} {69,2} {15}
/assign {itstf} {70,2} {15}
/assign {itstf} {71,2} {15}
/assign {itstf} {72,2} {15}
/assign {itstf} {73,2} {15}
/assign {itstf} {74,2} {16}
/assign {itstf} {75,2} {16}
/assign {itstf} {76,2} {16}
/assign {itstf} {77,2} {16}
/assign {itstf} {78,2} {16}
/assign {itstf} {79,2} {17}
/assign {itstf} {80,2} {17}
/assign {itstf} {81,2} {17}
/assign {itstf} {82,2} {17}
/assign {itstf} {83,2} {17}
/assign {itstf} {84,2} {18}
/assign {itstf} {85,2} {18}
/assign {itstf} {86,2} {18}
/assign {itstf} {87,2} {18}
/assign {itstf} {88,2} {18}

#Item Quantities
/assign {itstf} {1,3} {11}
/assign {itstf} {2,3} {85}
/assign {itstf} {3,3} {34}
/assign {itstf} {4,3} {68}
/assign {itstf} {5,3} {28}
/assign {itstf} {6,3} {280}
/assign {itstf} {7,3} {112}
/assign {itstf} {8,3} {170}
/assign {itstf} {9,3} {34}
/assign {itstf} {10,3} {48}
/assign {itstf} {11,3} {42}
/assign {itstf} {12,3} {42}
/assign {itstf} {13,3} {56}
/assign {itstf} {14,3} {34}
/assign {itstf} {15,3} {113}
/assign {itstf} {16,3} {48}
/assign {itstf} {17,3} {113}
/assign {itstf} {18,3} {85}
/assign {itstf} {19,3} {18}
/assign {itstf} {20,3} {7}
/assign {itstf} {21,3} {11}
/assign {itstf} {22,3} {46}
/assign {itstf} {23,3} {13}
/assign {itstf} {24,3} {11}
/assign {itstf} {25,3} {42}
/assign {itstf} {26,3} {85}
/assign {itstf} {27,3} {34}
/assign {itstf} {28,3} {8}
/assign {itstf} {29,3} {14}
/assign {itstf} {30,3} {42}
/assign {itstf} {31,3} {42}
/assign {itstf} {32,3} {85}
/assign {itstf} {33,3} {17}
/assign {itstf} {34,3} {6}
/assign {itstf} {35,3} {20}
/assign {itstf} {36,3} {34}
/assign {itstf} {37,3} {21}
/assign {itstf} {38,3} {10}
/assign {itstf} {39,3} {11}
/assign {itstf} {40,3} {21}
/assign {itstf} {41,3} {7}
/assign {itstf} {42,3} {48}
/assign {itstf} {43,3} {24}
/assign {itstf} {44,3} {68}
/assign {itstf} {45,3} {37}
/assign {itstf} {46,3} {34}
/assign {itstf} {47,3} {11}
/assign {itstf} {48,3} {1}
/assign {itstf} {49,3} {42}
/assign {itstf} {50,3} {43}
/assign {itstf} {51,3} {56}
/assign {itstf} {52,3} {42}
/assign {itstf} {53,3} {28}
/assign {itstf} {54,3} {42}
/assign {itstf} {55,3} {85}
/assign {itstf} {56,3} {42}
/assign {itstf} {57,3} {85}
/assign {itstf} {58,3} {280}
/assign {itstf} {59,3} {45}
/assign {itstf} {60,3} {42}
/assign {itstf} {61,3} {28}
/assign {itstf} {62,3} {68}
/assign {itstf} {63,3} {56}
/assign {itstf} {64,3} {68}
/assign {itstf} {65,3} {42}
/assign {itstf} {66,3} {11}
/assign {itstf} {67,3} {11}
/assign {itstf} {68,3} {28}
/assign {itstf} {69,3} {11}
/assign {itstf} {70,3} {28}
/assign {itstf} {71,3} {24}
/assign {itstf} {72,3} {42}
/assign {itstf} {73,3} {68}
/assign {itstf} {74,3} {56}
/assign {itstf} {75,3} {85}
/assign {itstf} {76,3} {56}
/assign {itstf} {77,3} {140}
/assign {itstf} {78,3} {11}
/assign {itstf} {79,3} {47}
/assign {itstf} {80,3} {42}
/assign {itstf} {81,3} {56}
/assign {itstf} {82,3} {56}
/assign {itstf} {83,3} {70}
/assign {itstf} {84,3} {17}
/assign {itstf} {85,3} {17}
/assign {itstf} {86,3} {112}
/assign {itstf} {87,3} {34}
/assign {itstf} {88,3} {85}
