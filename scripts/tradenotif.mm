/action {You feel as if you're being watched by many eyes.} {$NotifType Warning!! Kobolds!} {tradenotif}
/action {You spy a multitude of tiny footprints in the dust near the road.} {$NotifType Warning!! Kobolds!} {tradenotif}
/action {Your scalp prickles at the sibilant sound of leaves rustling.} {$NotifType Warning!! Kobolds!} {tradenotif}
/action {You hear the creak of a bowstring as an unseen hand draws it back.} {$NotifType Warning!! Kobolds} {tradenotif}
/action {You get an uneasy feeling and glance over your shoulder... at nothing.} {$NotifType Warning!! Bandits!} {tradenotif}
/action {You glimpse a bandit dash by, just off the road!} {$NotifType Warning!! Bandits!} {tradenotif}
/action {You sense some movement to the side of the road.} {$NotifType Warning!! Bandits or Trolls!} {tradenotif}
/action {You realize that this road seems too quiet.} {$NotifType Warning!! Rogues!} {tradenotif}
/action {A loud *SNAP* of a footfall on a stick just off the road gives you pause.} {$NotifType Warning!! Rogues!} {tradenotif}
/action {Your instincts warn you of an ambush up ahead.} {$NotifType Warning!! Rogues!} {tradenotif}
/action {A flock of birds spring into the air just ahead.} $NotifType Warning!! Rogues!} {tradenotif}
/action {You notice that everything is too quiet.} {$NotifType Warning!! Dogs and Demon Lord oh my!} {tradenotif}
/action {Undescribable fear grips you, you struggle just to breath!} {$NotifType Warning!! Dogs and Demon Lord oh my!} {tradenotif}
/action {The loud crack of a dimensional vortex collapsing nearby sets your hairs on end.} {$NotifType Warning!! Dogs and Demon Lord oh my!} {tradenotif}
/action {Your heart starts pounding as you are overcome by fear, a Demon must be close by!} {$NotifType Warning!! Dogs and Demon Lord oh my!} {tradenotif}
/action {An awful scent nearby alerts you to a possible Troll attack.} {$NotifType Warning!! Trolls!} {tradenotif}
/action {You hear the bone chilling war cry of a horde of Trolls.} {$NotifType Warning!! Trolls!} {tradenotif}
/action {You step on what used to be a human. Trolls must have ripped him apart.} {$NotifType Warning!! Trolls!} {tradenotif}
/alias {trnotifhelp} {/showme {Trade notification help:};/showme {trnotifon: Turns trade notification messages on (default is off).};/showme {trnotifoff: Turns trade notifications off.};/showme {trsay: Makes you say trade notifications};/showme {trform: Makes you send trade notifications to form line. (default)};/showme {trstatus: Tells you if trade notification is on or off.};/showme {trnotiftype: Tells you if notification is set to form or say.}} {tradenotifcomm}
/alias {trnotifon} {/enablegroup tradenotif;/var {TRNotifStatus} {1}} {tradenotifcomm}
/alias {trnotifoff} {/disablegroup tradenotif;/var {TRNotifStatus} {0}} {tradenotifcomm}
/alias {trsay} {/var {TRNotifType} {say};/var {TRNotifTypeVar} {0};/showme {You will now say trade notifications.}} {tradenotifcomm}
/alias {trform} {/var {TRNotifType} {f};/var {TRNotifTypeVar} {1};/showme {You will now send trade notifications to your form.}} {tradenotifcomm}
/alias {trstatus} {/if {@Var(TRNotifStatus)=1} {/showme {Trade notifications are on.}} {/showme {Trade notifications are off.}} {tradenotifcomm}
/alias {trnotiftype} {/if {@Var(TRNotifTypeVar)=1} {/showme {Trade notification set to form.}} {/showme {Trade notification set to be said.}} {tradenotifcomm}
/var {TRNotifType} {f}
/var {TRNotifTypeVar} {1}
/var {TRNotifStatus} {0}
/disablegroup tradenotif
