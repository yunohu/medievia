/substitute {%0 immediate area, %1.} {$0 immediate area, $1 (0-1)@Chr(10)} {distances}
/substitute {%0 close by, %1.} {$0 close by, $1 (2-5)@Chr(10)} {distances}
/substitute {%0 not far from here, %1.} {$0 not far from here, $1 (5-10)@Chr(10)} {distances}
/substitute {%0, off in the distance, %1.} {$0, off in the distance, $1 (11-15)@Chr(10)} {distances}
/substitute {%0 several miles away, %1.} {$0 several miles away, $1 (16-25)@Chr(10)} {distances}
/substitute {%0, far off in the distance, %1.} {$0, far off in the distance, $1 (26-50)@Chr(10)} {distances}
/substitute {%0 tens of miles away in the distance, %1.} {$0 tens of miles away in the distance, $1 (51-75)@Chr(10)} {distances}
/substitute {%0 many miles away, %1.} {$0 many miles away, $1 (76-100)@Chr(10)} {distances}
/substitute {%0 a great distance away, %1.} {$0 a great distance away, $1 (101+)@Chr(10)} {distances}
/substitute {Clock in the immediate area.} {Clock in the immediate area. (0-1)@Chr(10)} {distances}
/substitute {Clock close by.} {Clock close by. (2-5)@Chr(10)} {distances}
/substitute {Clock not far from here.} {Clock not far from here. (5-10)@Chr(10)} {distances}
/substitute {Clock off in the distance.} {Clock  off in the distance. (11-15)@Chr(10)} {distances}
/substitute {Clock several miles away.} {Clock several miles away. (16-25)@Chr(10)} {distances}
/substitute {Clock far off in the distance.} {Clock far off in the distance. (26-50)@Chr(10)} {distances}
/substitute {Clock tens of miles away in the distance.} {Clock tens of miles away in the distance. (51-75)@Chr(10)} {distances}
/substitute {Clock many miles away.} {Clock many miles away. (76-100)@Chr(10)} {distances}
/substitute {Clock a great distance away.} {Clock a great distance away. (101+)@Chr(10)} {distances}
