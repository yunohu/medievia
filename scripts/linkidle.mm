/alias {idleon} {/showme {@Chr(10)@AnsiBold()@ForeGreen()Idle enabled};/enableevent IDLE;/var IDLEon 1} {idlegroup}
/alias {idleoff} {/showme {@Chr(10)@AnsiBold()@ForeGreen()Idle disabled};/disableevent IDLE;/var IDLEon 0} {idlegroup}
/alias {idlehelp} {/showme {@Chr(10)@AnsiBold()@ForeRed()idle makes you send a return every 90 seconds, so you don't go idle@Chr(10)@AnsiBold()@ForeYellow()idleon--Turn on Idling@Chr(10)@AnsiBold()@ForeYellow()idleoff--Turn off Idling@Chr(10)@AnsiBold()@ForeYellow()idlestat--Tells you if idling is on or off}} {idlegroup}
/alias {idlestat} {/if {@Var(IDLEon)=1} {/showme {@Chr(10)@AnsiBold()@ForeGreen()IDLE IS ON}} {/showme {@Chr(10)@AnsiBold()@ForeYellow()IDLE IS OFF}} {idlegroup}
/variable {IDLEon} {0} {idlegroup}
/event {IDLE} {90} {@Chr(10)} {idlegroup}
/disableevent {IDLE}
