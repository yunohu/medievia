/alias {spup} {c sense life; c detect invis; c detect evil; c detect good} {}
/alias {tt} {c transport} {}
/alias {pp %0} {c plague $0} {}
/alias {ab} {c acid blast} {}
/alias {mi} {c mass invis} {}
/alias {sw %0} {c shockwave $0} {}
/alias {sr} {c shield room} {}
/alias {mq} {c mass quickness} {}
/alias {mr} {c mass refresh} {}
/alias {cc %0} {c cure critic $0} {}
/alias {dm %0} {c dispel magic $0} {}
/alias {ed %0} {c energy drain $0} {}
/alias {cf %0} {c farsight $0} {}
/alias {orb} {hold sanctuary;use sanctuary;hold $HoldItem} {}

