/action {^You flee in a near-blind panic.} {sneak} {sneakers}
/action {^You stop sneaking.} {sneak} {sneakers}
/action {^You receive %0 experience points from the battle} {sneak} {sneakers}
/action {^You are awarded %0 experience points} {sneak} {sneakers}
/alias {sneakoff} {/disablegroup sneakers}
/alias {sneakon} {/enablegroup sneakers}
