/loadlib {MMMisc} {Extlibs\MM Misc.dll}
/calldll {MMMisc} {Effects} {ANTI-CLERIC,!C}
/calldll {MMMisc} {Effects} {ANTI-MAGE,!M}
/calldll {MMMisc} {Effects} {ANTI-THIEF,!T}
/calldll {MMMisc} {Effects} {ANTI-WARRIOR,!W}
/calldll {MMMisc} {Attributes} {BACKSTABBER,BSer}
/calldll {MMMisc} {Attributes} {DAGGER,DG}
/calldll {MMMisc} {Effects} {NO-DONATE,ND}
/calldll {MMMisc} {Effects} {NO-DROP,NDr}
/calldll {MMMisc} {Effects} {NO-SACRIFICE,NS}
/action {^Your divination is complete...} {/calldll {MMMisc} {ItemClear};/emp {EggedAura};/emp {DaysLeft}} {id}
/action {^Object: %0 [%1]} {/calldll {MMMisc} {ItemName} {$0}} {id}
/action {^Item Type: %0   Effects: %1} {/calldll {MMMisc} {ItemType} {$0};/calldll {MMMisc} {ItemEffects} {$1}} {id}
/action {^Equipable Location(s): TAKE%0} {/if {@IsEmpty(@LTrim($0)) == 1} {/calldll {MMMisc} {ItemLocations} {NONE}} {/calldll {MMMisc} {ItemLocations} {@LTrim($0)}}} {id}
/action {^Weight: %0 Value: %1 Level Restriction: %2} {/calldll {MMMisc} {ItemWeight} {$0};/calldll {MMMisc} {ItemValue} {$1};/calldll {MMMisc} {ItemLevel} {$2}} {id}
/action {The object appears to be in perfect pristine condition.} {/calldll {MMMisc} {ItemCondition} {pristine}} {id}
/action {The object appears to be in excellent condition.} {/calldll {MMMisc} {ItemCondition} {excellent}} {id}
/action {The object appears to be in good condition.} {/calldll {MMMisc} {ItemCondition} {good}} {id}
/action {The object appears to be in fair condition.} {/calldll {MMMisc} {ItemCondition} {fair}} {id}
/action {The object is in fair condition but has some scratches.} {/calldll {MMMisc} {ItemCondition} {scratched}} {id}
/action {The object clearly shows major signs of wear and tear.} {/calldll {MMMisc} {ItemCondition} {worn}} {id}
/action {The object is visibly worn down with major wear.} {/calldll {MMMisc} {ItemCondition} {worn down}} {id}
/action {The life of this object is clearly coming to an end soon.} {/calldll {MMMisc} {ItemCondition} {ending}} {id}
/action {The object looks as if it will fall apart any day now.} {/calldll {MMMisc} {ItemCondition} {any day}} {id}
/action {The object is visibly crumbling and decaying....} {/calldll {MMMisc} {ItemCondition} {crumbling}} {id}
/action {This object has been blessed by the Gods and seems indestructable.} {/calldll {MMMisc} {ItemCondition} {nodet}} {id}
/action {^It is surrounded by a bright, green aura.} {/var {EggedAura} { - bright green aura}} {id}
/action {^It is surrounded by a faint, green aura.} {/var {EggedAura} { - faint green aura}} {id}
/action {^It is surrounded by a flickering green aura.} {/var {EggedAura} { - flickering green aura}} {id}
/action {^It is surrounded by a black aura.} {/var {EggedAura} { - black aura}} {id}
/action {^Days Left: %0} {/v {DaysLeft} {@RTrim($0)}} {id}
/action {^AC-apply of %0 } {/calldll {MMMisc} {ItemAcap} {$0}} {id}
/action {^Attributes: %0} {/calldll {MMMisc} {ItemAttributes} {$0}} {id}
/action {Class Restrictions: %0} {/calldll {MMMisc} {ItemAttributes} {$0}} {id}
/action {^Damage Dice of %0} {/calldll {MMMisc} {ItemDice} {$0}} {id}
/action {^Level %0 spell of %1.  Holds %2 charges and has %3 charges left.} {/calldll {MMMisc} {ItemSpellLevel} {$0};/calldll {MMMisc} {ItemSpellName} {$1};/calldll {MMMisc} {ItemCharges} {$3,$2}} {id}
/action {^Level %0 spells of %1} {/calldll {MMMisc} {ItemSpellLevel} {$0};/calldll {MMMisc} {ItemSpellName} {@RTrim($1)};/calldll {MMMisc} {ItemCharges} {1,1}} {id}
/action {^If eaten, this will produce the effects of the %0 spell.} {/calldll {MMMisc} {ItemSpellName} {$0};/calldll {MMMisc} {ItemSpellLevel} {?};/calldll {MMMisc} {ItemCharges} {1,1}} {id}
/action {^Regenerates level %0 spell of %1.  Has %2 maximum charges.} {/calldll {MMMisc} {ItemSpellLevel} {$0};/calldll {MMMisc} {ItemSpellName} {$1};/calldll {MMMisc} {ItemCharges} {?,$2}} {id}
/action {^    %0 to %1} {/calldll {MMMisc} {ItemAffect} {$1,$0} } {id}
/alias {chid} {/calldll {MMMisc} {ItemCustom} {/emotea @AnsiReset()@ForeMagenta()reports on @Var(IdLine)}} {id}
/alias {clanid} {/calldll {MMMisc} {ItemCustom} {clan @StripAnsi(@Var(IdLine))}} {id}
/alias {formid} {/calldll {MMMisc} {ItemCustom} {f @StripAnsi(@Var(IdLine))}} {id}
/alias {idhelp %0} {/if {@IsEmpty($0)} {/calldll {MMMisc} {Help};/showme {@Chr(10)@AnsiReset()@AnsiBold()@ForeGreen()Help for id};/showme {@AnsiReset()@AnsiBold()@ForeWhite()===========};/showme {@Chr(10)@AnsiReset()@AnsiBold()@ForeCyan()chid @AnsiReset() - Chat ID information to all chat connections.};/showme {@AnsiReset()@AnsiBold()@ForeCyan()clanid@AnsiReset() - Send ID information to clan.};/showme {@AnsiReset()@AnsiBold()@ForeCyan()formid@AnsiReset() - Send ID information to your form.};/showme {@AnsiReset()@AnsiBold()@ForeCyan()sayid@AnsiReset() - Say the ID information.};/showme {@AnsiReset()@AnsiBold()@ForeCyan()shoutid@AnsiReset() - Shout the ID information.};/showme {@AnsiReset()@AnsiBold()@ForeCyan()showid@AnsiReset() - Show yourself the ID information.};/showme {@AnsiReset()@AnsiBold()@ForeCyan()townid@AnsiReset() - Send the ID information to the town.};/showme {@AnsiReset()@AnsiBold()@ForeCyan()tellid @ForeYellow()<@ForeRed()person@ForeYellow()>@AnsiReset() - tell <person> the ID information.};/showme {@AnsiReset()@AnsiBold()@ForeCyan()immid@AnsiReset() - Send ID Information to imm line.}} {/calldll {MMMisc} {Help} {$0}}} {id}
/alias {sayid} {/calldll {MMMisc} {ItemCustom} {say @StripAnsi(@Var(IdLine))}} {id}
/alias {shoutid} {/calldll {MMMisc} {ItemCustom} {shout @StripAnsi(@Var(IdLine))}} {id}
/alias {showid} {/calldll {MMMisc} {ItemCustom} {/showme {@Chr(10)@Var(IdLine)}} {id}
/alias {townid} {/calldll {MMMisc} {ItemCustom} {town @StripAnsi(@Var(IdLine))}} {id}
/alias {tellid %1} {/calldll {MMMisc} {ItemCustom} {telepath $1 @StripAnsi(@Var(IdLine))}} {id}
/alias {immid} {/calldll {MMMisc} {ItemCustom} {imm @StripAnsi(@Var(IdLine))}} {id}
/emp {EggedAura} {id}
/emp {DaysLeft} {id}
/var {IdLine} {@AnsiReset()@AnsiBold()@ForeGreen()[@ForeWhite()%n@ForeGreen()]@ForeWhite(): @AnsiReset()@ForeMagenta()Lev@AnsiBold()@ForeGreen()[@ForeWhite()%l@AnsiBold()@ForeGreen()] @AnsiReset()@ForeMagenta()Loc@AnsiBold()@ForeGreen()[@ForeWhite()%L@AnsiBold()@ForeGreen()] @AnsiReset()@ForeMagenta()Ac@AnsiBold()@ForeGreen()[@ForeWhite()%A@AnsiBold()@ForeGreen()] @ForeWhite()%d@AnsiReset()@ForeMagenta()d@AnsiBold()@ForeWhite()%D @AnsiReset()@ForeMagenta()%f@AnsiBold()@ForeGreen()[@ForeWhite()%m@AnsiBold()@ForeGreen()] @AnsiReset()@ForeMagenta()%F@AnsiBold()@ForeGreen()[@ForeWhite()%M@AnsiBold()@ForeGreen()] @AnsiBold()@ForeGreen()[@ForeWhite()%a@AnsiBold()@ForeGreen()] @AnsiBold()@ForeGreen()[@ForeWhite()%e@AnsiBold()@ForeGreen()] @AnsiReset()@ForeMagenta()Cond@AnsiBold()@ForeGreen()[@ForeWhite()%c\@Var(EggedAura)@AnsiBold()@ForeGreen()] @AnsiReset()@ForeMagenta()Days@AnsiBold()@ForeGreen()[@ForeWhite()\@Var(DaysLeft)@AnsiBold()@ForeGreen()] @AnsiReset()@ForeMagenta()Spell@AnsiBold()@ForeGreen()[@ForeWhite()%s@ForeGreen():@ForeWhite()%S @AnsiReset()@ForeMagenta()Chgs@AnsiBold()@ForeGreen():@ForeWhite()%h@AnsiReset()@ForeMagenta()/@AnsiBold()@ForeWhite()%H@ForeGreen()] @ForeWhite()} {id}
