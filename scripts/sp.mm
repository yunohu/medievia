/action {The battle distracts you from your spell casting.} {ResetSpellup} {grpSpellup}
/action {You chant out the arcane words and are drained of %1 mana.} {/if {$bSpellup} {CastNextSpell}} {grpSpellup}
/action {You have not practiced that spell} {/if {$bSpellup} {CastNextSpell}} {grpSpellup}
/action {Sorry, you can't do that.} {/if {$bSpellup} {CastNextSpell}} {grpSpellup}
/action {You dream of accomplishing such great and wonderous feats.} {ResetSpellup} {grpSpellup}
/action {You fumble over the correct inflection} {/if {$bSpellup} {CastSameSpell}} {grpSpellup}
/alias {CastFirstSpell} {/var {nSpellIndex} {1};/if {$spstat} {/showme {@ForeMagenta()Casting @ForeCyan()@GetItem(lstSpells,$nSpellIndex)@ForeMagenta()...}};@GetItem(lstSpells,$nSpellIndex)} {grpSpellup}
/alias {CastNextSpell} {/math {nSpellIndex} {$nSpellIndex + 1};/if {$nSpellIndex <= @GetCount(lstSpells)} {@GetItem(lstSpells,$nSpellIndex);/if {$spstat} {/showme {@ForeGreen()SUCCESS@ForeMagenta(), Next is --> @ForeCyan()@GetItem(lstSpells,$nSpellIndex)@Chr(10)}}} {ResetSpellup}} {grpSpellup}
/alias {CastSameSpell} {@GetItem(lstSpells,$nSpellIndex);/if {$spstat} {/showme {@ForeRed()FAILED@ForeMagenta(), Recasting --> @ForeCyan()@GetItem(lstSpells,$nSpellIndex)}}} {grpSpellup}
/alias {ResetSpellup} {/if {$bSpellup} {/showme {@ForeYellow()Spellup has been reset.};/var bSpellup 0;/var nSpellIndex 0}} {grpSpellup}
/alias {rs} {ResetSpellup} {grpSpellup}
/alias {sp} {/var {bSpellup} {1};CastFirstSpell} {grpSpellup}
/alias {sp+} {/enablegroup grpSpellup} {}
/alias {sp-} {/disablegroup grpSpellup} {grpSpellup}
/alias {spadd %0} {/itemadd {lstSpells} {$0}} {grpSpellup}
/alias {spdel %0} {/itemdel {lstSpells} {$0}} {grpSpellup}
/alias {sphelp} {/showme {@Chr(10)@AnsiBold()@ForeRed()Commands  - - - - - -  What they do... @Chr(10)-------------------------------------------------------@Chr(10)@AnsiBold()@ForeMagenta()Sphelp@AnsiBold()@ForeRed()  - - - - - - -  @AnsiBold()@ForeGreen()This listing@Chr(10)@AnsiBold()@ForeMagenta()Sp@AnsiBold()@ForeRed()  - - - - - - - - -  @AnsiBold()@ForeGreen()Begins casting spells@Chr(10)@AnsiBold()@ForeMagenta()Spshow@AnsiBold()@ForeRed()  - - - - - - -  @AnsiBold()@ForeGreen()Displays Spells in the List@Chr(10)@AnsiBold()@ForeMagenta()Spadd <xx>@AnsiBold()@ForeRed()  - - - - -  @AnsiBold()@ForeGreen()Adds spell <xx> to the List@Chr(10)@AnsiBold()@ForeMagenta()Spdel <xx>@AnsiBold()@ForeRed()  - - - - -  @AnsiBold()@ForeGreen()Removes spell <xx> from the List@Chr(10)@AnsiBold()@ForeMagenta()Sp-@AnsiBold()@ForeRed()  - - - - - - - - - @AnsiBold()@ForeGreen()Disables the entire group of commands@Chr(10)@AnsiBold()@ForeMagenta()Sp+@AnsiBold()@ForeRed()  - - - - - - - - - @AnsiBold()@ForeGreen()Enables group of commands@Chr(10)@AnsiBold()@ForeMagenta()Spstat@AnsiBold()@ForeRed()  - - - - - - -  @AnsiBold()@ForeGreen()Toggles the status of spell casting}} {grpSpellup}
/alias {spshow} {/listitem lstSpells} {grpSpellup}
/alias {spstat} {/if {$spstat} {/var spstat 0;/showme {@AnsiBold()@ForeCyan()Spell Info @AnsiBold()@ForeYellow()OFF}} {/var spstat 1;/showme {@AnsiBold()@ForeCyan()Spell Info @AnsiBold()@ForeBlue()ON}}} {grpSpellup}
/variable {bSpellWasCast} {0} {grpSpellup}
/variable {bSpellup} {0} {}
/variable {nSpellIndex} {0} {grpSpellup}
/variable {showroom} {0} {}
/variable {spstat} {1} {}
/variable {test} {1} {}
/listadd {lstSpells} {grpSpellup}
/itemadd {lstSpells} {c armor}
/itemadd {lstSpells} {c bless}
/itemadd {lstSpells} {c breathe}
/itemadd {lstSpells} {c detect evil}
/itemadd {lstSpells} {c detect good}
/itemadd {lstSpells} {c detect invis}
/itemadd {lstSpells} {c detect magic}
/itemadd {lstSpells} {c infra}
/itemadd {lstSpells} {c mass invis}
/itemadd {lstSpells} {c lev}
/itemadd {lstSpells} {c protection}
/itemadd {lstSpells} {c quickness}
/itemadd {lstSpells} {c sense life}
/itemadd {lstSpells} {c shield}
#/itemadd {lstSpells} {c stone}
#/itemadd {lstSpells} {c str}

