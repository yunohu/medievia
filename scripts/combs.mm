/alias {setlight %0} {/if {@IsEmpty($0)==1 || @WordCount($0)!=1} {/showme {"setlight <lightsource>" - This will setup the variable to automatically remove your lightsource when combing.  <lightsource> is a one word ID that your light source responds to.  eg: "setlight hourglass"}} {/var LightSource $0;/showme {Light Source set to $LightSource}}} {combs}
/alias {setcontainer %0} {/if {@IsEmpty($0)==1 || @WordCount($0)!=1} {/showme {"setcontainer <container>" - This will setup the default container for putting eggs into.}} {/var Container $0;/showme {Container set to $Container}}} {combs}
/action {A Large Section of Tunnel, Covered with Strange Formations} {/if {$rootActive = 0} {remove $LightSource}} {combs}
/action {^From far away the sounds of wind rumble the area} {sit} {combs}
/action {^The sound of Gushing wind gets your attention.} {sit} {combs}
/macro {f6} {remove $LightSource} {combs}
/action {You receive %0 experience points from the battle of} {hold $Container;get all.egg corpse;put all.egg $Container;hold $HoldItem} {combs}
/action {You wince in pain as flying darts pierce your neck and face.} {remove $LightSource} {combs}
/action {A fairly large egg is here...humming quietly} {get all.egg;put all.egg $Container} {combs}
/action {You are awarded %0 experience points for the battle} {hold $Container;get all.egg corpse;put all.egg $Container;hold $HoldItem} {combs}
/alias {disableautogetgroup} {/disableaction {You receive %0 experience points from the battle of}} {combs}
/alias {enableautogetgroup} {/enableaction {You receive %0 experience points from the battle of}} {combs}
/alias {disableautoget} {/disableaction {You are awarded %0 experience points for the battle}} {combs}
/alias {enableautoget} {/enableaction {You are awarded %0 experience points for the battle}} {combs}
/alias {disablegets} {/disableaction {You receive %0 experience points from the battle of};/disableaction {You are awarded %0 experience points for the battle}} {combs}
/alias {enablegets} {/enableaction {You receive %0 experience points from the battle of};/enableaction {You are awarded %0 experience points for the battle}} {combs}
/event {COMBSSPAM} {4} {COMBSSPAM;/disableevent COMBSSPAM} {combs}
/alias {COMBSSPAM} {/showme {CombsInfo@Chr(10)1. Please use setlight <lightsource>.@Chr(10)2. Please setcontainer <Your container>.}} {combs}/disableaction {You are awarded %0 experience points for the battle}/disableaction {You receive %0 experience points from the battle of}
/var {rootActive} {0} {combs}
/action {That tasted rather strange!!} {/var rootActive {1}} {combs}
/action {You choke and cough, spitting up some greenish-brown mucus.} {/var rootActive {0}} {combs}
/action {repaerdnim root     : Duration} {/var rootActive {1}} {combs}
/alias {combshelp} {/showme {@Chr(10)@AnsiBold()@ForeYellow()Combs version 1.5 by @ForeRed()Kernighan@AnsiReset()@Chr(10)}} {combs}
disablegets
disableautoget
