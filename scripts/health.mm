/substitute {%0 barely clings to life.} {$0 barely clings to life. (1%-7%)@Chr(10)} {condition}
/substitute {%0 grimaces in pain.} {$0 grimaces in pain. (48%-54%)@Chr(10)} {condition}
/substitute {%0 grimaces with pain.} {$0 grimaces with pain. (48%-54%)@Chr(10)} {condition}
/substitute {%0 has a few scratches.} {$0 has a few scratches. (90%-99%)@Chr(10)} {condition}
/substitute {%0 has a nasty looking welt on the forehead.} {$0 has a nasty looking welt on the forehead. (83%-89%)@Chr(10)} {condition}
/substitute {%0 has many grievous wounds.} {$0 has many grievous wounds. (22%-28%)@Chr(10)} {condition}
/substitute {%0 has quite a few wounds.} {$0 has quite a few wounds. (55%-61%)@Chr(10)} {condition}
/substitute {%0 has some large, gaping wounds.} {$0 has some large, gaping wounds. (36%-40%)@Chr(10)} {condition}
/substitute {%0 has some minor wounds.} {$0 has some minor wounds. (69%-75%)@Chr(10)} {condition}
/substitute {%0 has some nasty wounds and bleeding cuts.} {$0 has some nasty wounds and bleeding cuts. (41%-47%)@Chr(10)} {condition}
/substitute {%0 has some big nasty wounds and scratches.} {$0 has some big nasty wounds and scratches. (41%-47%)@Chr(10)} {condition}
/substitute {%0 has some small wounds and bruises.} {$0 has some small wounds and bruises. (76%-82%)@Chr(10)} {condition}
/substitute {%0 is covered with blood from oozing wounds.} {$0 is covered in blood from oozing wounds. (15%-21%)@Chr(10)} {condition}
/substitute {%0 is in excellent condition.} {$0 is in excellent condition. (100%)@Chr(10)} {condition}
/substitute {%0 is vomiting blood.} {$0 is vomiting blood. (15%-21%)@Chr(10)} {condition}
/substitute {%0 looks pretty awful.} {$0 looks pretty awful. (29%-35%)@Chr(10)} {condition}
/substitute {%0 pales visibly as Death nears.} {$0 pales visibly as death nears. (8%-14%)@Chr(10)} {condition}
/substitute {%0 pales visibly as death nears.} {$0 pales visibly as death nears. (8%-14%)@Chr(10)} {condition}
/substitute {%0 screams in agony.} {$0 screams in agony. (22%-28%)@Chr(10)} {condition}
/substitute {%0 winces in pain.} {$0 winces in pain. (62%-68%)@Chr(10)} {condition}
