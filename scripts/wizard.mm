/action {^Marious, the Great Wizard telepaths you, 'Let's see here... I see an entrance at...'} {/enableaction {^Marious, the Great Wizard telepaths you, '%0'}} {combst}
/action {^Marious, the Great Wizard telepaths you, '%0'} {/var RoomLoc $0;/disableaction {^Marious, the Great Wizard telepaths you, '%0'}} {combst}
/action {^Marious, the Great Wizard telepaths you, 'in the general vicinity of'} {/enableaction {^Marious, the Great Wizard telepaths you, '%1'}} {combst}
/action {^Marious, the Great Wizard telepaths you, '%1'} {/var ZoneLoc $1;/disableaction {^Marious, the Great Wizard telepaths you, '%1'};/if {$ZoneLoc ="Van"} {/var ZoneLoc {Van'Klyn's Estate}} {}} {combst}
/action {^Marious, the Great Wizard telepaths you, 'The entrance %0'} {/var CombsAge $0;} {combst}

/alias {combsinfo} {/showme {Combs [Zone] $ZoneLoc [Room] $RoomLoc [Age] $CombsAge}} {combst}
/alias {clcombs} {clan Combs [Zone] $ZoneLoc [Room] $RoomLoc [Age] The entrance $CombsAge} {combst}
/alias {saycombs} {say Combs [Zone] $ZoneLoc [Room] $RoomLoc [Age] The entrance $CombsAge} {combst}
/alias {towncombs} {town Combs [Zone] $ZoneLoc [Room] $RoomLoc [Age] The entrance $CombsAge} {combst}
/disableaction {The wizard: %0} {combst}
/disableaction {The wizard: %1} {combst}
/disableaction {^The wizard telepaths you, '%0'} {combst}
/disableaction {^The wizard telepaths you, '%1'} {combst}
/var {RoomLoc} {None} {combst}
/var {ZoneLoc} {None} {combst}
/var {CombsAge} {None} {combst}
/showme {Kernighan's Wiz Script V1.2} {combst}
