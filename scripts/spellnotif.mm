/action {The white glow around %0's body fades} {$NotifType $0's sanctuary has been dispelled} {spellnotif}
/action {The shield of fire around %0's body fizzles} {$NotifType $0's fireshield has been dispelled} {spellnotif}
/action {The white aura around your body fades} {$NotifType ..ooOO Sanc Out OOoo..} {spellnotif}
/action {%0 chokes and utters some muffled noises.} {$NotifType $0 has been plagued} {spellnotif}
/action {%0's illusionary duplicates vanish with a bright spark of energy.} {$NotifType $0's phantasmal images has been dispelled.} {spellnotif}
/action {%0 seems to be blinded!} {$NotifType $0 has been blinded.} {spellnotif}
/action {%0 seems weaker} {$NotifType $0 has been weakened.} {spellnotif}
/action {%0 turns %1 eyes away and screams!} {$NotifType $0 has been blinded.} {spellnotif}
/action {%0 briefly reveals a red aura!} {$NotifType $0 has been cursed.} {spellnotif}
/action {%0 winces as the poison enters %1 body} {$NotifType $0 has been poisoned.} {spellnotif}
/alias {spellnotifhelp} {/showme {Spell notification help:};/showme {spellnotifon: Turns spell notification messages on (default is off).};/showme {spellnotifoff: Turns spell notifications off.};/showme {spellsay: Makes you say spell notifications};/showme {spellform: Makes you send spell notifications to form line. (default)};/showme {spellstatus: Tells you if spell notification is on or off.};/showme {spellnotiftype: Tells you if notification is set to form or say.}} {spellnotifcomm}
/alias {spellnotifon} {/enablegroup spellnotif;/var {NotifStatus} {1}} {spellnotifcomm}
/alias {spellnotifoff} {/disablegroup spellnotif;/var {NotifStatus} {0}} {spellnotifcomm}
/alias {spellsay} {/var {NotifType} {say};/var {NotifTypeVar} {0};/showme {You will now say spell notifications.}} {spellnotifcomm}
/alias {spellform} {/var {NotifType} {f};/var {NotifTypeVar} {1};/showme {You will now send spell notifications to your form.}} {spellnotifcomm}
/alias {spellstatus} {/if {@Var(NotifStatus)=1} {/showme {Spell notifications are on.}} {/showme {Spell notifications are off.}} {spellnotifcomm}
/alias {spellnotiftype} {/if {@Var(NotifTypeVar)=1} {/showme {Spell notification set to form.}} {/showme {Spell notification set to be said.}} {spellnotifcomm}
/var {NotifType} {f}
/var {NotifTypeVar} {1}
/var {NotifStatus} {0}
/disablegroup spellnotif
