/alias {amagicoff} {/disablegroup AutoMagic;/showme {@Chr(10)@AnsiReset()@ForeBlue()[@AnsiBold()@ForeCyan()AutoMagic @ForeRed()DISABLED!@AnsiReset()@ForeBlue()]};emote has disabled catering to the formation!} {amagicoff}
/alias {amagicon} {/enablegroup AutoMagic;/showme {@Chr(10)@AnsiReset()@ForeBlue()[@AnsiBold()@ForeCyan()AutoMagic @ForeGreen()ENABLED!@AnsiReset()@ForeBlue()]};emote puts on the chef boyardee hat and caters to the formation!} {amagicoff}

/alias {amagic %9} {/if {@IsEmpty($9)==1} {/var AutoMagicSpam {@Chr(10)@AnsiBold()@ForeGreen()Auto-Magic Spells:};/loop {1,20} {/if {@InList(AutoMagicCurrent,@GetItem(AutoMagicMasterList,$LoopCount))==0} {/var AutoMagicSpam {@ConCat($AutoMagicSpam,@Chr(10)@AnsiBold()@ForeWhite()@PadLeft($LoopCount, ,@Math(2-@Len($LoopCount)))@ForeBlack()[@ForeRed()X@ForeBlack()] @AnsiReset()@ForeBlue()@GetItem(AutoMagicMasterList,$LoopCount))}} {/var AutoMagicSpam {@ConCat($AutoMagicSpam,@Chr(10)@AnsiBold()@ForeWhite()@PadLeft($LoopCount, ,@Math(2-@Len($LoopCount)))@ForeBlack()[@ForeYellow()@Chr(251)@ForeBlack()] @ForeGreen()@GetItem(AutoMagicMasterList,$LoopCount))}}};/showme {@Var(AutoMagicSpam)@Chr(10)@AnsiBold()@ForeBlack()"amagic #" to toggle spells. "SAM" to spam spells}} {/if {@IsNumber($9)==0} {/showme {@Chr(10)@AnsiBold()@ForeYellow()"amagic <Number to toggle>" ie: amagic 15 would toggle Remove_Poison on/off@AnsiReset()}} {/if {$9 >=1 && $9 <= 20} {/if {@InList(AutoMagicCurrent,@GetItem(AutoMagicMasterList,$9))==0} {/itemadd AutoMagicCurrent @GetItem(AutoMagicMasterList,$9);amagic} {/itemdel AutoMagicCurrent @GetItem(AutoMagicMasterList,$9);amagic}} {/showme {@Chr(10)@AnsiBold()@ForeYellow()The Number must be from 1-20! Try again...}}}}} {AutoMagic}

/alias {SAM} {/empty AutoMagicFormSpam;@CommandToList(AutoMagicCurrent,/var AutoMagicFormSpam {@ConCat($AutoMagicFormSpam, $ListItem)});/if {@WordCount($AutoMagicFormSpam)<11} {fsay AutoMagic: @Var(AutoMagicFormSpam)} {fsay AutoMagic: @Word($AutoMagicFormSpam,1) @Word($AutoMagicFormSpam,2) @Word($AutoMagicFormSpam,3) @Word($AutoMagicFormSpam,4) @Word($AutoMagicFormSpam,5) @Word($AutoMagicFormSpam,6) @Word($AutoMagicFormSpam,7) @Word($AutoMagicFormSpam,8) @Word($AutoMagicFormSpam,9) @Word($AutoMagicFormSpam,10);/empty temp;/loop {11,@GetCount(AutoMagicCurrent)} {/var temp {@ConCat($temp,@GetItem(AutoMagicCurrent,$LoopCount) )}};fsay @Var(temp)}} {AutoMagic}

/action {%0 tells the formation, '%1'.} {/if {@InList(AutoMagicSpammers,]$1)==1} {/if {$1 = "Ar" && @InList(AutoMagicCurrent,Armor)==1} {c armor $0} {/if {$1 = "Bl" && @InList(AutoMagicCurrent,Bless)==1} {c bless $0} {/if {$1 = "BW" && @InList(AutoMagicCurrent,Breath_Water)==1} {c breath $0} {/if {$1 = "CB" && @InList(AutoMagicCurrent(Cure_Blind)==1} {c cure blind $0} {/if {$1 = "CC" && @InList(AutoMagicCurrent,Cure_Critic)==1} {c cure critic $0} {/if {$1 = "Fo" && @InList(AutoMagicCurrent,Food)==1} {/2 c create food;give food $0} {/if {$1 = "He" && @InList(AutoMagicCurrent,Heal)==1} {c heal $0} {/if {$1 = "In" && @InList(AutoMagicCurrent,Invisibility)==1} {c invis $0} {/if {$1 = "Le" && @InList(AutoMagicCurrent,Levitate)==1} {c lev $0} {/if {$1 = "MI" && @InList(AutoMagicCurrent,Mass_Invisibility)==1} {c mass invis} {/if {$1 = "ML" && @InList(AutoMagicCurrent,Mass_Levitate)==1} {c mass levitate} {/if {$1 = "MQ" && @InList(AutoMagicCurrent,Mass_Quickness)==1} {c mass quick} {/if {$1 = "MR" && @InList(AutoMagicCurrent,Mass_Refresh)==1} {c mass refresh} {/if {$1 = "Qu" && @InList(AutoMagicCurrent,Quickness)==1} {c quick $0} {/if {$1 = "Re" && @InList(AutoMagicCurrent,Refresh)==1} {c refresh $0} {/if {$1 = "RP" && @InList(AutoMagicCurrent,Remove_Poison)==1} {c remove poison $0} {/if {$1 = "Sa" && @InList(AutoMagicCurrent,Sanctuary)==1} {c sanc $0} {/if {$1 = "SR" && @InList(AutoMagicCurrent,Shield_Room)==1} {c shield room} {/if {$1 = "Su" && @InList(AutoMagicCurrent,Summon)==1} {c summon $0} {/if {$1 = "Tr" && @InList(AutoMagicCurrent,Transport)==1} {c trans}}}}}}}}}}}}}}}}}}}}}} {AutoMagic}

/action {[FORM] %0 tells everyone, '%1'.} {/if {@InList(AutoMagicSpammers,]$1)==1} {/if {$1 = "Ar" && @InList(AutoMagicCurrent,Armor)==1} {c armor $0} {/if {$1 = "Bl" && @InList(AutoMagicCurrent,Bless)==1} {c bless $0} {/if {$1 = "BW" && @InList(AutoMagicCurrent,Breath_Water)==1} {c breath $0} {/if {$1 = "CB" && @InList(AutoMagicCurrent(Cure_Blind)==1} {c cure blind $0} {/if {$1 = "CC" && @InList(AutoMagicCurrent,Cure_Critic)==1} {c cure critic $0} {/if {$1 = "Fo" && @InList(AutoMagicCurrent,Food)==1} {/2 c create food;give food $0} {/if {$1 = "He" && @InList(AutoMagicCurrent,Heal)==1} {c heal $0} {/if {$1 = "In" && @InList(AutoMagicCurrent,Invisibility)==1} {c invis $0} {/if {$1 = "Le" && @InList(AutoMagicCurrent,Levitate)==1} {c lev $0} {/if {$1 = "MI" && @InList(AutoMagicCurrent,Mass_Invisibility)==1} {c mass invis} {/if {$1 = "ML" && @InList(AutoMagicCurrent,Mass_Levitate)==1} {c mass levitate} {/if {$1 = "MQ" && @InList(AutoMagicCurrent,Mass_Quickness)==1} {c mass quick} {/if {$1 = "MR" && @InList(AutoMagicCurrent,Mass_Refresh)==1} {c mass refresh} {/if {$1 = "Qu" && @InList(AutoMagicCurrent,Quickness)==1} {c quick $0} {/if {$1 = "Re" && @InList(AutoMagicCurrent,Refresh)==1} {c refresh $0} {/if {$1 = "RP" && @InList(AutoMagicCurrent,Remove_Poison)==1} {c remove poison $0} {/if {$1 = "Sa" && @InList(AutoMagicCurrent,Sanctuary)==1} {c sanc $0} {/if {$1 = "SR" && @InList(AutoMagicCurrent,Shield_Room)==1} {c shield room} {/if {$1 = "Su" && @InList(AutoMagicCurrent,Summon)==1} {c summon $0} {/if {$1 = "Tr" && @InList(AutoMagicCurrent,Transport)==1} {c trans}}}}}}}}}}}}}}}}}}}}}} {AutoMagic}

/variable {temp} {null} {temp}
/variable {AutoMagicSpam} {null} {AutoMagic}
/variable {AutoMagicFormSpam} {null} {AutoMagic}
/empty {AutoMagicSpam} {AutoMagic}
/listadd {AutoMagicCurrent} {AutoMagic}
/listadd {AutoMagicSpammers} {AutoMagic}
/itemadd {AutoMagicSpammers} {]Ar}
/itemadd {AutoMagicSpammers} {]Bl}
/itemadd {AutoMagicSpammers} {]BW}
/itemadd {AutoMagicSpammers} {]CB}
/itemadd {AutoMagicSpammers} {]CC}
/itemadd {AutoMagicSpammers} {]Fo}
/itemadd {AutoMagicSpammers} {]He}
/itemadd {AutoMagicSpammers} {]In}
/itemadd {AutoMagicSpammers} {]Le}
/itemadd {AutoMagicSpammers} {]MI}
/itemadd {AutoMagicSpammers} {]ML}
/itemadd {AutoMagicSpammers} {]MQ}
/itemadd {AutoMagicSpammers} {]MR}
/itemadd {AutoMagicSpammers} {]Qu}
/itemadd {AutoMagicSpammers} {]Re}
/itemadd {AutoMagicSpammers} {]RP}
/itemadd {AutoMagicSpammers} {]Sa}
/itemadd {AutoMagicSpammers} {]SR}
/itemadd {AutoMagicSpammers} {]Su}
/itemadd {AutoMagicSpammers} {]Tr}
/listadd {AutoMagicMasterList} {AutoMagic}
/itemadd {AutoMagicMasterList} {Armor}
/itemadd {AutoMagicMasterList} {Bless}
/itemadd {AutoMagicMasterList} {Breath_Water}
/itemadd {AutoMagicMasterList} {Cure_Blind}
/itemadd {AutoMagicMasterList} {Cure_Critic}
/itemadd {AutoMagicMasterList} {Food}
/itemadd {AutoMagicMasterList} {Heal}
/itemadd {AutoMagicMasterList} {Invisibility}
/itemadd {AutoMagicMasterList} {Levitate}
/itemadd {AutoMagicMasterList} {Mass_Invisibility}
/itemadd {AutoMagicMasterList} {Mass_Levitate}
/itemadd {AutoMagicMasterList} {Mass_Quickness}
/itemadd {AutoMagicMasterList} {Mass_Refresh}
/itemadd {AutoMagicMasterList} {Quickness}
/itemadd {AutoMagicMasterList} {Refresh}
/itemadd {AutoMagicMasterList} {Remove_Poison}
/itemadd {AutoMagicMasterList} {Sanctuary}
/itemadd {AutoMagicMasterList} {Shield_Room}
/itemadd {AutoMagicMasterList} {Summon}
/itemadd {AutoMagicMasterList} {Transport}
