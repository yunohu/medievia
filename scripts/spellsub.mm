/sub {%0 chants the magical phrase, 'Bet Sanct'} {$0 chants the magical phrase, 'armor'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Wis Quas Lor'} {$0 chants the magical phrase, 'detect invisibility'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Wis Mani'} {$0 chants the magical phrase, 'sense life'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Kal Ex Jux'} {$0 chants the magical phrase, 'protection from evil'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Wis Ort'} {$0 chants the magical phrase, 'detect magic'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Kal Por'} {$0 chants the magical phrase, 'quickness'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Uus Por'} {$0 chants the magical phrase, 'levitate'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Flam Ort Grav'} {$0 chants the magical phrase, 'fireshield'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Vas Sanct Grav'} {$0 chants the magical phrase, 'sanctuary'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'In Quas Xen'} {$0 chants the magical phrase, 'phantasmal image'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'waia Ort Grav'} {$0 chants the magical phrase, 'mana shield'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Sanct Ort'} {$0 chants the magical phrase, 'bless'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Vas Uus Por'} {$0 chants the magical phrase, 'mass levitation'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Vas Kal Por'} {$0 chants the magical phrase, 'mass quickness'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Vas An Corp Kal Tym Manir'} {$0 chants the magical phrase, 'resurrect'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Ort Grav'} {$0 chants the magical phrase, 'shield'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Ylem Hur'} {$0 chants the magical phrase, 'sandstorm'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Ort Sanct Ylem'} {$0 chants the magical phrase, 'stone skin'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Ort Jux'} {$0 chants the magical phrase, 'magic missile'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Vas Corp Hur'} {$0 chants the magical phrase, 'shockwave'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'In Vas Grav'} {$0 chants the magical phrase, 'hammer of faith'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'In Corp'} {$0 chants the magical phrase, 'harm'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Por An Flam'} {$0 chants the magical phrase, 'frost shards'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Kal Flam'} {$0 chants the magical phrase, 'flamestrike'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Vas Mani'} {$0 chants the magical phrase, 'heal'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Wis Jux'} {$0 chants the magical phrase, 'detect evil'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Kol Morah'} {$0 chants the magical phrase, 'scry'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Ban Por'} {$0 chants the magical phrase, 'attune'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Kal Vas Mani'} {$0 chants the magical phrase, 'shared life'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Vas Rel Corp Mani'} {$0 chants the magical phrase, 'blood bath'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'In An Ort Ylem'} {$0 chants the magical phrase, 'minor creation'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Ort Ex Por'} {$0 chants the magical phrase, 'word of recall'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Vas Ort Grav'} {$0 chants the magical phrase, 'shield room'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Vas Nox Hur'} {$0 chants the magical phrase, 'acid blast'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Vas Por Ylem'} {$0 chants the magical phrase, 'tremor'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Flam Hur'} {$0 chants the magical phrase, 'firestorm'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Jux Mani'} {$0 chants the magical phrase, 'cause critic'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Vas Nox'} {$0 chants the magical phrase, 'plague'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Ort Hur'} {$0 chants the magical phrase, 'colour spray'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'An Lor'} {$0 chants the magical phrase, 'blindness'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Quas Flam'} {$0 chants the magical phrase, 'faerie fire'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Jux Ort'} {$0 chants the magical phrase, 'curse'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Quas Corp'} {$0 chants the magical phrase, 'fear'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Grav Jux'} {$0 chants the magical phrase, 'shocking grasp'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Nox'} {$0 chants the magical phrase, 'poison'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'In Mani'} {$0 chants the magical phrase, 'cure serious'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'An Mani'} {$0 chants the magical phrase, 'cause serious'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Kal Zu'} {$0 chants the magical phrase, 'sleep'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Quas Lor'} {$0 chants the magical phrase, 'invisibility'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Kal Xen'} {$0 chants the magical phrase, 'conjure elemental'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Wis Ort Ylem'} {$0 chants the magical phrase, 'identify'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Kal Ex Mani'} {$0 chants the magical phrase, 'protection from good'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'In Yarl'} {$0 chants the magical phrase, 'create food'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Vas Bet Ex'} {$0 chants the magical phrase, 'mass refresh'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Vas Quas Lor'} {$0 chants the magical phrase, 'mass invisibility'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Bet Ex'} {$0 chants the magical phrase, 'refresh'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Quas Wis Por'} {$0 chants the magical phrase, 'farsight'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Ex Yorl'} {$0 chants the magical phrase, 'breathe water'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Por Xen'} {$0 chants the magical phrase, 'phase'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Vas Tu'} {$0 chants the magical phrase, 'strength'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Nu Wis'} {$0 chants the magical phrase, 'infravision'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'An Ort'} {$0 chants the magical phrase, 'dispel magic'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Wis Ylem'} {$0 chants the magical phrase, 'locate object'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Vas Xen Por'} {$0 chants the magical phrase, 'summon'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Bet An Mani'} {$0 chants the magical phrase, 'cause light'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Jux Grav Hur'} {$0 chants the magical phrase, 'lightning bolt'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'In Lor'} {$0 chants the magical phrase, 'continual light'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'An Flam'} {$0 chants the magical phrase, 'chill touch'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Ex Por Ylem'} {$0 chants the magical phrase, 'ethereal'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Por Flam'} {$0 chants the magical phrase, 'fireball'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Kal Bet Flam'} {$0 chants the magical phrase, 'burning hands'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'An Jux'} {$0 chants the magical phrase, 'dispel evil'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Kal Corp Flam'} {$0 chants the magical phrase, 'demonfire'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'In Vas Hur'} {$0 chants the magical phrase, 'hands of wind'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Ort Por'} {$0 chants the magical phrase, 'teleport'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Vas Ex Por'} {$0 chants the magical phrase, 'transport'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'An Nox'} {$0 chants the magical phrase, 'remove poison'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Des Por'} {$0 chants the magical phrase, 'weaken'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Wis Xen'} {$0 chants the magical phrase, 'know alignment'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Bet Mani'} {$0 chants the magical phrase, 'cure light'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Wis Nox'} {$0 chants the magical phrase, 'detect poison'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Wis Corp'} {$0 chants the magical phrase, 'sense death'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Kal Mani'} {$0 chants the magical phrase, 'cure critic'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'An Quas'} {$0 chants the magical phrase, 'faerie fog'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Rel Ort Ylem'} {$0 chants the magical phrase, 'enchant weapon'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Vas Hur Jux Grav'} {$0 chants the magical phrase, 'chain lightning'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Wis Des Ylem'} {$0 chants the magical phrase, 'map catacombs'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Rel Wis'} {$0 chants the magical phrase, 'scribe'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Kal Jux Grav'} {$0 chants the magical phrase, 'call lightning'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Ban Vas Corp'} {$0 chants the magical phrase, 'malediction'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Grav Des'} {$0 chants the magical phrase, 'energy drain'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'An Jux Ort'} {$0 chants the magical phrase, 'remove curse'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'In Yorl'} {$0 chants the magical phrase, 'create water'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Rel Xen'} {$0 chants the magical phrase, 'charm person'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Por Quas Wis'} {$0 chants the magical phrase, 'wizard eye'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Uus Ex Por'} {$0 chants the magical phrase, 'gate'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Mani An Lor'} {$0 chants the magical phrase, 'cure blindness'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Wis Vati'} {$0 chants the magical phrase, 'sense presence'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Wis Flam'} {$0 chants the magical phrase, 'sense fire'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Wis Por Hur'} {$0 chants the magical phrase, 'sense weather'@Chr(10)} {spellsub}
/sub {%0 chants the magical phrase, 'Wis Okt'} {$0 chants the magical phrase, 'sense movement'@Chr(10)} {spellsub}
/showme {Kernighan's SpellSub Script V1.3} {combst}
