/substitute {%0 barely touches %1!} {$0 barely touches $1! (1-2)@Chr(10)} {damage}
/substitute {%0 scratches %1!} {$0 scratches $1! (3)@Chr(10)} {damage}
/substitute {%0 bruises %1!} {$0 bruises $1! (4)@Chr(10)} {damage}
/substitute {%0 hits %1!} {$0 hits $1! (5)@Chr(10)} {damage}
/substitute {%0 injures %1!} {$0 injures $1! (6-7)@Chr(10)} {damage}
/substitute {%0 wounds %1!} {$0 wounds $1 (8-9)@Chr(10)} {damage}
/substitute {%0 draws blood from %1!} {$0 draws blood from $1! (10-11)@Chr(10)} {damage}
/substitute {%0 smites %1!} {$0 smites $1! (12-14)@Chr(10)} {damage}
/substitute {%0 massacres %1!} {$0 massacres $1! (15-20)@Chr(10)} {damage}
/substitute {%0 decimates %1!} {$0 decimates $1! (21-25)@Chr(10)} {damage}
/substitute {You decimate %1!} {You decimate $1! (21-25)@Chr(10)} {damage}
/substitute {%0 devastates %1!} {$0 devastates $1! (26-30)@Chr(10)} {damage}
/substitute {%0 maims a %1!} {$0 maims a $1! (31-40)@Chr(10)} {damage}
/substitute {%0 maims the %1!} {$0 maims the $1! (31-40)@Chr(10)} {damage}
/substitute {%0 maims you!} {$0 maims you! (31-40)@Chr(10)} {damage}
/substitute {%0 mutilates %1!} {$0 mutiliates $1! (41-50)@Chr(10)} {damage}
/substitute {%0 pulverizes %1!} {$0 pulverizes $1! (51-60)@Chr(10)} {damage}
/substitute {%0 demolishes %1!} {$0 demolishes $1! (61-70)@Chr(10)} {damage}
/substitute {%0 mangles %1!} {$0 mangles $1! (71-80)@Chr(10)} {damage}
/substitute {%0 obliterates %1!} {$0 obliterates $1! (81-90)@Chr(10)} {damage}
/substitute {%0 annihilates %1!} {$0 annihilates $1! (91-100)@Chr(10)} {damage}
/substitute {%0 horribly maims %1!} {$0 horribly maims $1! (101-130)@Chr(10)} {damage}
/substitute {%0 visciously rends %1!} {$0 viciously rends $1! (131+)@Chr(10)} {damage}
/substitute {%0 dismembers %1!} {$0 dismembers $1! (24%-30%)@Chr(10)} {damage}
/substitute {%0 eviscerates %1!} {$0 eviscerates $1! (31%-50%)@Chr(10)} {damage}
/substitute {%0 disembowels %1!} {$0 disembowels $1! (51%-75%)@Chr(10)} {damage}
/substitute {%0 decapitates %1!} {$0 decapitates $1! (76%-99%)@Chr(10)} {damage}
