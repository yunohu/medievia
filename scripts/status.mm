/action {A deep red shield of fire surrounds you.} {/baritemfore {fir} {11}} {status}
/action {A glowing white aura surrounds your body.} {/baritemfore {san1} {11};/baritemfore {fir} {1}} {status}
/action {A spectral image of a raven emerges from your body, flying away.} {/baritemfore {bls1} {1}} {status}
/action {A spectral raven emerges from your body, forced out by magic.} {/baritemfore {bls1} {1}} {status}
/action {A suit of ethereal armor appears on you and slowly vanishes.} {/baritemfore {mag1} {11}} {status}
/action {A warm feeling runs through your body.} {/baritemfore {poison}{1}} {status}
/action {Restarted by %0} {allreset} {status}
/action {You focus and become ONE with the catacombs.....POOF} {/baritemfore {map}{11}} {status}
/action {The detect invisible wears off.} {/baritemfore {dei1} {1}} {status}
/action {The detect magic wears off.} {/baritemfore {dem1} {1}} {status}
/action {The spectral image of a raven soars down from the Heavens, disappearing into your body.} {/var {mag} {1};/baritemfore {bls1} {11}} {status}
/action {The white aura around your body fades.} {/baritemfore {san1} {1}} {status}
/action {You are surrounded by a strong magical force shield.} {/baritemfore {Shl} {11}} {status}
/action {You can no longer detect magic.} {/baritemfore {dem1} {1}} {status}
/action {You can no longer see in the dark.} {/baritemfore {inf1} {1}} {status}
/action {Your sight grows dimmer.} {/baritemfore {inf1} {1}} {status}
/action {You don't feel so well protected.} {/baritemfore {mag1} {1}} {status}
/action {You feel a cloak of blindness dissolve.} {/baritemfore {bli1} {1}} {status}
/action {You feel less aware of your surroundings.} {/baritemfore {sen1} {1}} {status}
/action {Your purple mapping aura fades and leaves you exhausted.} {/baritemfore {map} {1}} {status}
/action {You feel less in touch with living things.} {/baritemfore {sen1} {1}} {status}
/action {You feel less perceptive.} {/baritemfore {dei1} {1}} {status}
/action {You feel less protected.} {/baritemfore {mag1}{1}} {status}
/action {You feel more vulnerable.} {/baritemfore {pro1} {1}} {status}
/action {You feel stronger.} {/baritemfore {weak}{1}} {status}
/action {You feel better now.} {/baritemfore {plague}{1}} {status}
/action {You feel the power of vile darkness surge through your body!} {/baritemfore {pro1} {5}} {status}
/action {You feel the protective veil of darkness vanish.} {/baritemfore {pro1} {1}} {status}
/action {You feel very sick.} {/baritemfore {poison}{14}} {status}
/action {You choke and utter some muffled noises.} {/baritemfore {plague}{14}} {status}
/action {You feel weak} {/baritemfore {weak}{14}} {status}
/action {You feel your awareness improve.} {/baritemfore {sen1}{11}} {status}
/action {You feel your lungs harden and heat up!} {/baritemfore {bre1} {11}} {status}
/action {You feel your stamina increase greatly.} {/baritemfore {qui1} {11}} {status}
/action {You feel yourself exposed.} {/baritemfore {invis} {1}} {status}
/action {You gasp and breath in} {/baritemfore {bre1} {11}} {status}
/action {You gasp and breathe in} {/baritemfore {bre1} {11}} {status}
/action {You get a bright ball of light from the corpse} {drop ball} {status}
/action {You glow with a pulsating, white light for a few moments.} {/baritemfore {pro1} {11}} {status}
/action {You have been KILLED!} {allreset} {status}
/action {You have been KILLED!!} {allreset} {status}
/action {You sense the red in your vision disappear} {/baritemfore {dem1} {1}} {status}
/action {You sense the red in your vision disappear.} {/baritemfore {dem1}{1}} {status}
/action {You slowly vanish from normal vision.} {/baritemfore {invis}{11}} {status}
/action {Your Mana Shield crackles, shrinks momentarily and dissipates.} {/baritemfore {man} {1}} {status}
/action {Your ability to detect evil auras has been dispelled.} {/baritemfore {dem1} {1}} {status}
/action {Your comrades obstruct your path to the melee.} {/baritemfore {invis}{1}} {status}
/action {Your crush %0} {/baritemfore {invis}{1}} {status}
/action {Your endurance returns to normal.} {/baritemfore {qui1} {1}} {status}
/action {Your eyes glow dimly for a few seconds.} {/baritemfore {dei1} {11}} {status}
/action {Your eyes glow red for a moment.} {/baritemfore {inf1} {11}} {status}
/action {Your eyes tingle.} {/baritemfore {dem1} {11}} {status}
/action {Your feel your awareness improve.} {/baritemfore {sen1} {11}} {status}
/action {Your feet rise up off the ground.} {/baritemfore {lev1} {11}} {status}
/action {Your force shield shimmers then fades away.} {/baritemfore {Shl} {1}} {status}
/action {Your levitation wears off.} {/baritemfore {lev1} {1}} {status}
/action {You don't feel lighter than air any more.} {/baritemfore {lev1} {1}} {status}
/action {Your lungs feel like they are on fire} {/baritemfore {bre1} {1}} {status}
/action {Your lungs heave and you cough water.} {/baritemfore {bre1} {1}} {status}
/action {Your magical energy expands and hardens into a crackling blue shield which surrounds you.} {/baritemfore {man} {11}} {status}
/action {Your magical shield has been dispelled.} {/baritemfore {Shl} {1} {}} {status}
/action {Your pierce %0} {/baritemfore {invis}{1}} {status}
/action {Your pound %0} {/baritemfore {invis}{1}} {status}
/action {Your protective veil of darkness has been shattered.} {/baritemfore {pro1} {1}} {status}
/action {Your shield of fire collapses.} {/baritemfore {fir} {1}} {status}
/action {Your shield of fire slowly dissipates....and blinks out.} {/baritemfore {fir} {1}} {status}
/action {Your skin does not feel as hard as it used to.} {/baritemfore {Stn} {1}} {status}
/action {Your skin feels like stone no longer.} {/baritemfore {Stn} {1}} {status}
/action {Your skin turns to a stone-like substance.} {;/baritemfore {Stn} {11}} {status}
/action {Your slash %0} {/baritemfore {invis}{1}} {status}
/action {%0 mutilates you} {/baritemfore {invis}{1}} {status}
/action {You rush into the fray!} {/baritemfore {invis}{1}} {status}
/action {Your vision returns!} {/baritemfore {bli1} {1}} {status}
/action {Your white aura flashes brightly and disappears} {/baritemfore {san1} {1}} {status}
/action {Your white aura flashes brightly and vanishes} {/baritemfore {san1} {1}} {status}
/action {armor               : Duration} {/baritemfore {mag1}{11}} {status}
/action {bless               : Duration} {/baritemfore {bls1}{11}} {status}
/action {blinded you!} {/baritemfore {bli1} {14}} {status}
/action {blindness           : Duration} {/baritemfore {bli1}{14}} {status}
/action {breathe water       : Duration} {/baritemfore {bre1}{11}} {status}
/action {detect evil         : Duration} {/baritemfore {dem1}{11}} {status}
/action {detect invisibility : Duration} {/baritemfore {dei1}{11}} {status}
/action {detect magic        : Duration} {/baritemfore {dem1}{11}} {status}
/action {fireshield          : Duration} {/baritemfore {fir}{11}} {status}
/action {infravision         : Duration} {/baritemfore {inf1}{11}} {status}
/action {invisibility        : Duration} {/baritemfore {invis}{11}} {status}
/action {levitate            : Duration} {/baritemfore {lev1}{11}} {status}
/action {mana shield         : Duration} {/baritemfore {man}{11}} {status}
/action {protection from evil: Duration} {/baritemfore {pro1}{11}} {status}
/action {protection from good: Duration} {/baritemfore {pro1}{5}} {status}
/action {quickness           : Duration} {/baritemfore {qui1}{11}} {status}
/action {sanctuary           : Duration} {/baritemfore {san1}{11}} {status}
/action {sense life          : Duration} {/baritemfore {sen1}{11}} {status}
/action {shield              : Duration} {/baritemfore {Shl}{11}} {status}
/action {stone skin          : Duration} {/baritemfore {Stn}{11}} {status}
/action {map catacombs       : Duration} {/baritemfore {map}{11}} {status}
/action {plague              : Duration} {/baritemfore {plague}{14}} {status}
/action {weaken              : Duration} {/baritemfore {weak}{14}} {status
/alias {allreset} {/baritemfore {invis} {1};/baritemfore {Shl} {1};/baritemfore {map} {1};/baritemfore {man} {1};/baritemfore {fir} {1};/baritemfore {Stn} {1};/baritemfore {san1} {1};/baritemfore {bls1} {1};/baritemfore {mag1} {1};/baritemfore {bli1} {1};/baritemfore {inf1} {1};/baritemfore {pro1} {1};/baritemfore {bre1} {1};/baritemfore {lev1} {1};/baritemfore {dei1} {1};/baritemfore {dem1} {1};/baritemfore {sen1} {1};/baritemfore {qui1} {1};/baritemfore {poison} {1};/baritemfore {plague} {1};/baritemfore {weak} {1}} {status}
/event {timer1} {60} {/updatebaritem {tim1};/updatebaritem {tim2}} {status}
/barseparator {plasep} {8} {status}
/baritem {plague} {Pl} {9} {2} {1} {0} {status}
/barseparator {wksep} {11} {status}
/baritem {weak} {Wk} {12} {2} {1} {0} {status}
/barseparator {sep22} {14} {status}
/baritem {bli1} {Bd} {15} {2} {1} {0} {status}
/barseparator {sep14} {17} {status}
/baritem {poison} {Ps} {18} {2} {1} {0} {status}
/barseparator {sep20} {20} {status}
/baritem {invis} {Iv} {21} {2} {1} {0} {status}
/barseparator {sep19} {23} {status}
/baritem {man} {MS} {24} {2} {1} {0} {status}
/barseparator {Sep18} {26} {status}
/baritem {fir} {FS} {27} {2} {1} {0} {status}
/barseparator {sep17} {29} {status}
/baritem {Stn} {St} {30} {2} {1} {0} {status}
/barseparator {sep21} {32} {status}
/baritem {Shl} {Sh} {33} {2} {1} {0} {status}
/barseparator {sep2} {35} {status}
/baritem {mag1} {Ar} {36} {2} {1} {0} {status}
/barseparator {sep3} {38} {status}
/baritem {bls1} {Bl} {39} {2} {1} {0} {status}
/barseparator {sep4} {41} {status}
/baritem {san1} {Sa} {42} {2} {1} {0} {status}
/barseparator {sep5} {44} {status}
/baritem {inf1} {In} {45} {2} {1} {0} {status}
/barseparator {sep6} {47} {status}
/baritem {bre1} {Br} {48} {2} {1} {0} {status}
/barseparator {sep8} {50} {status}
/baritem {lev1} {Lv} {51} {2} {1} {0} {status}
/barseparator {sep9} {53} {status}
/baritem {map} {MC} {54} {2} {1} {0} {status}
/barseparator {sep7} {56} {status}
/baritem {qui1} {Qu} {57} {2} {1} {0} {status}
/barseparator {sep10} {59} {status}
/baritem {pro1} {Pr} {60} {2} {1} {0} {status}
/barseparator {sep11} {62} {status}
/baritem {dei1} {DI} {63} {2} {1} {0} {status}
/barseparator {sep12} {65} {status}
/baritem {dem1} {DM/DE} {66} {5} {1} {0} {status}
/barseparator {sep15} {71} {status}
/baritem {sen1} {SL} {72} {2} {1} {0} {status}
/barseparator {tme} {74} {status}
/baritem {tim1} {@Hour()} {75} {2} {6} {0} {status}
/baritem {hash} {:} {77} {1} {6} {0} {status}
/baritem {tim2} {@Minute()} {78} {2} {6} {0} {status}
/barseparator {end} {80} {status}
